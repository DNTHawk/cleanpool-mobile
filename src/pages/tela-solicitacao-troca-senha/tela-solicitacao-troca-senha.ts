import { Component } from '@angular/core';
import { IonicPage, ModalController, AlertController, MenuController, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Http, Response } from '@angular/http';
import header from './../../utils/headers'


@IonicPage()
@Component({
  selector: 'page-tela-solicitacao-troca-senha',
  templateUrl: 'tela-solicitacao-troca-senha.html',
})
export class TelaSolicitacaoTrocaSenhaPage {

  public form: FormGroup
  protected solicitacao: any

  constructor(private fb: FormBuilder, public modalCtrl: ModalController, private alertCtrl: AlertController, private menu: MenuController, public navCtrl: NavController, public http: Http ) {
    this.form = this.fb.group({
      email: ['', Validators.compose([
        Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
        Validators.required])],
    })
  }

  ionViewDidLoad() {
  }

  enviarEmail(){

    this.solicitacao={
      value : this.form.controls.email.value,
    }

    console.log(this.solicitacao);
    

    this.realizarUpdate()
  }

  private async realizarUpdate(): Promise<any> {
    return this.http.post('http://cleanpoolbackend.clicksistemas.com.br/usuario/recuperarsenha',
      JSON.stringify(this.solicitacao), { headers: header })
      .subscribe(
        (response: Response) => {
          console.log(response)
          let alert = this.alertCtrl.create(
            {
              message: 'Você receberá um email contendo uma senha temporaria',
              buttons: ['OK']
            }
          )
          alert.present()
          this.navCtrl.setRoot('TelaLoginPage');
        },
        (error: Error) => {
          console.log(error)
        }
      )
  }

}

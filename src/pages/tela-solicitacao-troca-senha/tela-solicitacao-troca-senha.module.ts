import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TelaSolicitacaoTrocaSenhaPage } from './tela-solicitacao-troca-senha';

@NgModule({
  declarations: [
    TelaSolicitacaoTrocaSenhaPage,
  ],
  imports: [
    IonicPageModule.forChild(TelaSolicitacaoTrocaSenhaPage),
  ],
})
export class TelaSolicitacaoTrocaSenhaPageModule {}

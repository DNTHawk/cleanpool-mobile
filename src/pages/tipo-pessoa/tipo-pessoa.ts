import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tipo-pessoa',
  templateUrl: 'tipo-pessoa.html',
})
export class TipoPessoaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  pessoaFisica() {
    this.navCtrl.push('CadastroClientePage');
  }
  pessoaJuridica() {
    this.navCtrl.push('CadastroClienteJuridicoPage');
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TipoPessoaPage } from './tipo-pessoa';

@NgModule({
  declarations: [
    TipoPessoaPage,
  ],
  imports: [
    IonicPageModule.forChild(TipoPessoaPage),
  ],
})
export class TipoPessoaPageModule {}

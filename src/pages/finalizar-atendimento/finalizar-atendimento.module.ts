import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinalizarAtendimentoPage } from './finalizar-atendimento';

@NgModule({
  declarations: [
    FinalizarAtendimentoPage,
  ],
  imports: [
    IonicPageModule.forChild(FinalizarAtendimentoPage),
  ],
})
export class FinalizarAtendimentoPageModule {}

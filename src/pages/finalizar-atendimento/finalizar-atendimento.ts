import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import header from './../../utils/headers'
import moment from 'moment'
import { BaseDadosOffProvider } from '../../providers/base-dados-off/base-dados-off';
import { Network } from '@ionic-native/network';

@IonicPage()
@Component({
  selector: 'page-finalizar-atendimento',
  templateUrl: 'finalizar-atendimento.html',
})
export class FinalizarAtendimentoPage {

  private API_URL = 'http://cleanpoolbackend.clicksistemas.com.br'
  public dadosTratamento: any
  public dadosAgenda: any
  public dadosServico: any
  private dadosAgendaAux: Array<any> = []
  public agenda: any
  public produtosSugeridos: any
  public servicosAtendidos: any
  public agendas: Array<any> = []
  public agendaFinalizada: Array<any> = []
  public agendaFinalizadas: Array<any> = []
  public listaProdutosSugeridos: Array<any> = []
  public listaServicosAtendidos: Array<any> = []
  private idAgenda: string
  private dataFinal: string
  private horaFinal: string
  private horaInicio: string
  private rota: string
  private observacao: string
  private quantidadeFotos : number
  private listaFotos : Array<any> = []
  private dadosPiscineiro: any
  public dadosUsuario: any

  public fotos: Array<any> = []

  public conexao: String = "Online"

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController,
    public http: Http, private toast: ToastController, public baseDadosOffProvider: BaseDadosOffProvider,
    public network: Network) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(true, 'piscineiro');
  }

  async ngOnInit(): Promise<any> {
    this.network.onDisconnect().subscribe(() => {
      this.conexao = "Offline"
    }, error => console.log(error));

    this.network.onConnect().subscribe(() => {
      this.conexao = "Online"
    }, error => console.log(error));

    if (this.conexao == "Online") {
      this.modoOnline()
    }else if (this.conexao == "Offline"){
      this.modoOffline()
    }

    let jsonTarefa1 = window.localStorage.getItem('dadosUsuario')
    this.dadosUsuario = JSON.parse(jsonTarefa1)
  }

  modoOnline(){
    this.horaFinal = moment().format('HH:mm')

    this.dataFinal = new Date().toISOString()

    let jsonTarefa = window.localStorage.getItem('dadosTratamento')
    this.dadosTratamento = JSON.parse(jsonTarefa)

    this.horaInicio = moment(this.dadosTratamento.dataInicio).format('HH:mm')

    let jsonTarefa3 = window.localStorage.getItem('dadosServico')
    this.dadosServico = JSON.parse(jsonTarefa3)

    this.observacao = this.dadosServico.observacao

    let jsonTarefa2 = window.localStorage.getItem('agendaSelecionada')
    this.dadosAgenda = JSON.parse(jsonTarefa2)

    let jsonTarefa4 = window.localStorage.getItem('produtosSugeridos')
    this.produtosSugeridos = JSON.parse(jsonTarefa4)
    this.idAgenda = this.dadosAgenda.idAgenda
    this.consultaDadosAgenda()

    let jsonTarefa5 = window.localStorage.getItem('fotos')
    this.listaFotos = JSON.parse(jsonTarefa5)

    let jsonTarefa6 = window.localStorage.getItem('quantidadeFotos')
    this.quantidadeFotos = JSON.parse(jsonTarefa6)

    let jsonTarefa7 = window.localStorage.getItem('dadosPiscineiro')

    this.dadosPiscineiro = JSON.parse(jsonTarefa7)
  }
  modoOffline(){
    this.horaFinal = moment().format('HH:mm')

    this.dataFinal = new Date().toISOString()

    let jsonTarefa = window.localStorage.getItem('dadosTratamento')
    this.dadosTratamento = JSON.parse(jsonTarefa)

    this.horaInicio = moment(this.dadosTratamento.dataInicio).format('HH:mm')

    let jsonTarefa3 = window.localStorage.getItem('dadosServico')
    this.dadosServico = JSON.parse(jsonTarefa3)

    this.observacao = this.dadosServico.observacao

    let jsonTarefa2 = window.localStorage.getItem('agendaSelecionada')
    this.dadosAgenda = JSON.parse(jsonTarefa2)

    let jsonTarefa4 = window.localStorage.getItem('produtosSugeridos')
    this.produtosSugeridos = JSON.parse(jsonTarefa4)
    this.idAgenda = this.dadosAgenda.idAgenda

    let jsonTarefa5 = window.localStorage.getItem('fotos')
    this.listaFotos = JSON.parse(jsonTarefa5)

    let jsonTarefa6 = window.localStorage.getItem('quantidadeFotos')
    this.quantidadeFotos = JSON.parse(jsonTarefa6)

    let jsonTarefa7 = window.localStorage.getItem('dadosPiscineiro')

    this.dadosPiscineiro = JSON.parse(jsonTarefa7)

    this.agenda = {
      id: this.dadosAgenda.idAgenda,
      observacao: this.observacao,
      datainicio: this.dadosTratamento.dataInicio,
      datatermino: this.dataFinal,
      status: "ATENDIDO"
    }

    this.produtosSugeridos = {
      idAgenda: this.dadosAgenda.idAgenda,
      items: !this.produtosSugeridos ? [] : this.produtosSugeridos,
    }

    this.servicosAtendidos ={
      idAgenda: this.dadosAgenda.idAgenda,
      servicos: this.dadosServico.servicos,
    }

    this.baseDadosOffProvider.insertFinalizarAgenda(this.agenda)

    this.baseDadosOffProvider.updateAgenda(this.agenda)

    this.baseDadosOffProvider.insertProdutosSugeridos(this.produtosSugeridos)

    this.baseDadosOffProvider.insertServicosAtendidos(this.servicosAtendidos)
  }

  private async consultaDadosAgenda(): Promise<any> {
    await this.http.get(`${this.API_URL}/query/agendaFindById?entityId=${this.idAgenda}`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.dadosAgendaAux = await JSON.parse(response._body).data
        this.agenda = {
          id: this.dadosAgendaAux[0].id,
          observacao: this.observacao,
          datainicio: this.dadosTratamento.dataInicio,
          datatermino: this.dataFinal,
          items: !this.produtosSugeridos ? [] : this.produtosSugeridos,
          servicos: this.dadosServico.servicos,
          status: "ATENDIDO"
        }

        await this.realizarUpdate()
      }, async (error: any) => {
        if (error.status === 0) {
          this.modoOffline()
        }
      })
  }

  private async realizarUpdate(): Promise<any> {
    return await this.http.put('http://cleanpoolbackend.clicksistemas.com.br/agenda/finalizar/',
      JSON.stringify(this.agenda), { headers: header })
      .subscribe(
        (response: Response) => {
          console.log(response)
        },
        (error: Error) => {
          console.log(error)
        }
      )
  }

  goToOtherPageAgenda() {
    localStorage.removeItem("agendaSelecionada")
    localStorage.removeItem("dadosPiscina")
    localStorage.removeItem("dadosTratamento")
    localStorage.removeItem("produtosSugeridos")
    localStorage.removeItem("dadosServico")

    this.navCtrl.setRoot('LoadingsPage');
  }

}

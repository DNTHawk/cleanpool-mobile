import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreLoadingPage } from './pre-loading';

@NgModule({
  declarations: [
    PreLoadingPage,
  ],
  imports: [
    IonicPageModule.forChild(PreLoadingPage),
  ],
})
export class PreLoadingPageModule {}

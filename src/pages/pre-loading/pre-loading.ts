import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pre-loading',
  templateUrl: 'pre-loading.html',
})
export class PreLoadingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  async ngOnInit(): Promise<any> {
    this.navCtrl.setRoot('LoadingsPage')
  }

}

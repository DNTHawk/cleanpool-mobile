import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroClienteJuridicoPage } from './cadastro-cliente-juridico';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    CadastroClienteJuridicoPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroClienteJuridicoPage),
    BrMaskerModule
  ],
})
export class CadastroClienteJuridicoPageModule {}

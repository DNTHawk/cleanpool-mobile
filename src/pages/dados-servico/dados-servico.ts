import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Http } from '@angular/http'
import header from './../../utils/headers'
import moment from 'moment'

@IonicPage()
@Component({
  selector: 'page-dados-servico',
  templateUrl: 'dados-servico.html',
})
export class DadosServicoPage {

  protected servicoSelecionado: Array<any>
  protected listaServicos: Array<any>
  protected dadosTratamento: any
  protected dadosPiscineiro: any
  private horaInicio: string
  public dadosUsuario: any

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, public http: Http) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(false, 'piscineiro');
  }

  async ngOnInit(): Promise<any> {

    let jsonTarefa1 = window.localStorage.getItem('dadosUsuario')
    this.dadosUsuario = JSON.parse(jsonTarefa1)

    let jsonTarefa = window.localStorage.getItem('dadosTratamento')
    this.dadosTratamento = JSON.parse(jsonTarefa)

    this.horaInicio = moment(this.dadosTratamento.dataInicio).format('HH:mm')

    this.consultaListaServicos()
  }

  private async consultaListaServicos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/servicoFindByParameter?parameter=&number=1&size=10`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaServicos = await JSON.parse(response._body).data
      })
  }

  goToOtherPage() {
    this.navCtrl.push('EstadoPiscinaPage');
  }

}

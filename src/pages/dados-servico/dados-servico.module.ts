import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DadosServicoPage } from './dados-servico';

@NgModule({
  declarations: [
    DadosServicoPage,
  ],
  imports: [
    IonicPageModule.forChild(DadosServicoPage),
  ],
})
export class DadosServicoPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-estado-piscina',
  templateUrl: 'estado-piscina.html',
})
export class EstadoPiscinaPage {
  protected dadosPiscina: any
  protected volumeAgua : number
  protected msgAguaLimpa : string
  protected msgAguaLevementeTurva : string
  protected msgAguaTurva: string
  protected msgAguaLevementeVerde : string
  protected msgAguaVerde : string

  async ngOnInit(): Promise<any> {
    let jsonTarefa = window.localStorage.getItem('dadosPiscina')
    this.dadosPiscina = JSON.parse(jsonTarefa)

    this.volumeAgua = this.dadosPiscina[0].volumeagua
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
  exibirAguaLimpa(){

    var displayAguaLimpa = document.getElementById("aguaLimpa").style.display

    if(displayAguaLimpa == "none"){

      document.getElementById("aguaLimpa").style.display = 'block';
      document.getElementById("iconAguaLimpaUp").style.display = "block"
      document.getElementById("iconAguaLimpaDown").style.display = "none"
      this.msgAguaLimpa = "Não há necessidade de Clarificante e Algicida Choque."
    }else{
      document.getElementById("aguaLimpa").style.display = 'none';
      document.getElementById("iconAguaLimpaUp").style.display = "none"
      document.getElementById("iconAguaLimpaDown").style.display = "block"
    }

    
  }

  exibirAguaLevementeTurva(){
    var displayAguaLevementeTurva = document.getElementById("aguaLevementeTurva").style.display

    if(displayAguaLevementeTurva == "none"){
      document.getElementById("aguaLevementeTurva").style.display = 'block';
      document.getElementById("iconAguaLevementeTurvaUp").style.display = "block"
      document.getElementById("iconAguaLevementeTurvaDown").style.display = "none"
      var ml = 7 * (this.volumeAgua / 1000)

      if(ml >= 1000){
        let x = ml / 1000
        let litros = x.toFixed(0)

        this.msgAguaLevementeTurva = "Adicionar "+ litros +"ml de Clarificante."
      }else{
        let x = ml.toFixed(0)

        this.msgAguaLevementeTurva = "Adicionar "+ x +"ml de Clarificante."
      }
    }else{
      document.getElementById("aguaLevementeTurva").style.display = 'none';
      document.getElementById("iconAguaLevementeTurvaUp").style.display = "none"
      document.getElementById("iconAguaLevementeTurvaDown").style.display = "block"
    }

  }

  exibirAguaTurva(){

    var displayAguaTurva = document.getElementById("aguaTurva").style.display

    if(displayAguaTurva == "none"){
      document.getElementById("aguaTurva").style.display = "block"
      document.getElementById("iconAguaTurvaUp").style.display = "block"
      document.getElementById("iconAguaTurvaDown").style.display = "none"
      
      var ml = 14 * (this.volumeAgua / 1000)
  
      if(ml >= 1000){
        let x = ml / 1000
        let litros = x.toFixed(0)
  
        this.msgAguaTurva = "Adicionar "+ litros +"ml de Clarificante."
      }else{
        let x = ml.toFixed(0)

        this.msgAguaTurva = "Adicionar "+ x +"ml de Clarificante."
      }
    }else{
      document.getElementById("aguaTurva").style.display = "none"
      document.getElementById("iconAguaTurvaUp").style.display = "none"
      document.getElementById("iconAguaTurvaDown").style.display = "block"
    }

  }

  exibirAguaLevementeVerde(){

    var displayAguaLevementeVerde = document.getElementById("aguaLevementeVerde").style.display
    
    if(displayAguaLevementeVerde == "none"){
      document.getElementById("aguaLevementeVerde").style.display = "block"
      document.getElementById("iconAguaLevementeVerdeUp").style.display = "block"
      document.getElementById("iconAguaLevementeVerdeDown").style.display = "none"
      
      var ml = 7 * (this.volumeAgua / 1000)
  
      if(ml >= 1000){
        let x = ml / 1000
        let litros = x.toFixed(0)
  
        this.msgAguaLevementeVerde = "Adicionar "+ litros +"ml de Algicida Choque."
      }else{
        let x = ml.toFixed(0)

        this.msgAguaLevementeVerde = "Adicionar "+ x +"ml de Algicida Choque."
      }
    }else{
      document.getElementById("aguaLevementeVerde").style.display = "none"
      document.getElementById("iconAguaLevementeVerdeUp").style.display = "none"
      document.getElementById("iconAguaLevementeVerdeDown").style.display = "block"
    }

  }

  exibirAguaVerde(){
    var displayAguaVerde = document.getElementById("aguaVerde").style.display
    
    if(displayAguaVerde == "none"){
      document.getElementById("aguaVerde").style.display = "block"
      document.getElementById("iconAguaVerdeUp").style.display = "block"
      document.getElementById("iconAguaVerdeDown").style.display = "none"
      
      var ml = 14 * (this.volumeAgua / 1000)
  
      if(ml >= 1000){
        let x = ml / 1000
        let litros = x.toFixed(0)
  
        this.msgAguaVerde = "Adicionar "+ litros +"ml de Algicida Choque."
      }else{
        let x = ml.toFixed(0)

        this.msgAguaVerde = "Adicionar "+ x +"ml de Algicida Choque."
      }
    }else{
      document.getElementById("aguaVerde").style.display = "none"
      document.getElementById("iconAguaVerdeUp").style.display = "none"
      document.getElementById("iconAguaVerdeDown").style.display = "block"
    }
  }

  goToOtherPage() {
    this.navCtrl.push('SugerirProdutosPage')
  }
  
}

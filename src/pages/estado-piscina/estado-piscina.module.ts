import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EstadoPiscinaPage } from './estado-piscina';

@NgModule({
  declarations: [
    EstadoPiscinaPage,
  ],
  imports: [
    IonicPageModule.forChild(EstadoPiscinaPage),
  ],
})
export class EstadoPiscinaPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tela-perfil',
  templateUrl: 'tela-perfil.html',
})
export class TelaPerfilPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(false, 'piscineiro');

  }

  ionViewDidLoad() {
    
  }

  goToOtherPageAgendaPiscineiro(){
    this.navCtrl.setRoot('LoadingsPage')
  }

  goToOtherPageCliente(){
    this.navCtrl.setRoot('AvaliacaoPage')
  }

  logOut(){
    localStorage.clear()   

    this.navCtrl.setRoot('TelaLoginPage');
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import header from './../../utils/headers';

@IonicPage()
@Component({
  selector: 'page-cadastro-cliente',
  templateUrl: 'cadastro-cliente.html',
})
export class CadastroClientePage {
  activeMenu: string;
  public cadastro: FormGroup;
  public dadosCadastro: any

  constructor(public http: Http, public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public menuCtrl: MenuController, private fb: FormBuilder) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(false, 'piscineiro');

    this.cadastro = this.fb.group({
      nome: ['', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])],
      email: ['', Validators.compose([
        Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
        Validators.required
      ])],
      numero: ['', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])],
      telefone: ['', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])],
      celular: ['', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])],
      login: ['', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])],
      senha: ['', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])],
      confirmacao: ['', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])],
      tipo: "FISICA"
    })
  }

  private async realizarUpdate(): Promise<any> {
    const exibeDialog = (message: string): void => {
      const alert = this.alertCtrl.create(
        {
          message: message,
          buttons: ['OK']
        }
      )
      alert.present()
    }

    return await this.http.post('http://cleanpoolbackend.clicksistemas.com.br/usuario/app',
      JSON.stringify(this.dadosCadastro), { headers: header })
      .subscribe((): void => {
        exibeDialog('Logo entraremos em contato para completar o seu cadastro e poder oferecer o melhor tratamento para sua piscina')
        this.navCtrl.setRoot('TelaLoginPage');
      },
        (error: any) => {
          const errorMessage = JSON.parse(error._body)
          if (error.status === 400) {
            exibeDialog(errorMessage.errors[0])
          } else if (error.status === 500) {
            exibeDialog('Senha fraca. Favor informar uma senha que contenha no mínimo 1 letra minúscula, uma maiúscula e um número com tamanho de 6 dígitos.')
          }
        }
      )
  }

  autenticarCadastro() {
    let { senha, confirmacao } = this.cadastro.controls;

    if (senha.value === confirmacao.value) {
      this.dadosCadastro = this.cadastro.value

      this.realizarUpdate()

    } else {
      let alert = this.alertCtrl.create(
        {
          message: 'Senhas não conferem!',
          buttons: ['OK']
        }
      )
      alert.present()
    }
  }
}

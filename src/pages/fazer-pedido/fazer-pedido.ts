import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, MenuController, AlertController } from 'ionic-angular'
import { Http, Response } from '@angular/http';
import header from './../../utils/headers'
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
// import { HomeClientePage } from '../home-cliente/home-cliente';

@IonicPage()
@Component({
  selector: 'page-fazer-pedido',
  templateUrl: 'fazer-pedido.html',
})
export class FazerPedidoPage {

  public myForm: FormGroup;

  private listaGruposProdutos: Array<any>
  private listaSubGruposProdutos: Array<any>
  private listaProdutos: Array<any>
  private listaProdutosState: Array<any> = []
  private dataFiltro: string
  public nomeCliente: string
  protected listaPiscinas: string[]
  private tipos : Array<string> = []
  protected data: string
  protected dadosPiscineiro: Array<any> = []
  protected dadosUsuario: any = []
  protected piscinaSelecionada: any = []
  protected solicitacao: any

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public navParams: NavParams, public http: Http, public menuCtrl: MenuController, private fb: FormBuilder) { 
    let jsonTarefa = window.localStorage.getItem('dadosUsuario')
    this.dadosUsuario = JSON.parse(jsonTarefa)
    this.tipos = this.dadosUsuario.tipos

    if(this.tipos.length > 1){
      this.menuCtrl.enable(false, 'cliente');
      this.menuCtrl.enable(true, 'clientePiscineiro');
    }else{
      this.menuCtrl.enable(true, 'cliente');
      this.menuCtrl.enable(false, 'clientePiscineiro');
    }
  }


  async ngOnInit(): Promise<any> {

    this.myForm = this.fb.group({
      produto: this.fb.array([])
    });

    let jsonTarefa = window.localStorage.getItem('piscinaSelecionada')
    this.piscinaSelecionada = JSON.parse(jsonTarefa)

    this.consultaListaProdutos()
    this.consultaGruposProdutos()
    this.consultaSubGruposProdutos()
  }

  onChange(produto: string, value: number) {
    const produtoFormArray = <FormArray>this.myForm.controls.produto;

    if (value) {
      produtoFormArray.push(new FormControl({
        id: produto,
        qtd: value
      }));
    } else {
      let index = produtoFormArray.controls.findIndex(x => x.value == produto)
      produtoFormArray.removeAt(index);
    }
  }

  private async consultaListaProdutos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/produtoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaProdutos = await JSON.parse(response._body).data
        this.listaProdutosState = this.listaProdutos
      })
  }

  private async consultaGruposProdutos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/grupoProdutoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaGruposProdutos = await JSON.parse(response._body).data
      })
  }

  private async consultaSubGruposProdutos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/subGrupoProdutoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaSubGruposProdutos = await JSON.parse(response._body).data
      })
  }

  filtraItens(evento: any) {
    this.listaProdutos = this.listaProdutosState
    const val = evento.target.value

    if (val && val.trim() !== '') {
      this.listaProdutos = this.listaProdutos.filter(produto => (produto.nome.toLowerCase().indexOf(val.toLowerCase()) > -1))
    }
  }

  logOut(){
    localStorage.clear()   

    this.navCtrl.setRoot('TelaLoginPage');
  }

  goToPageHome() {      
    let sugestoes = []
    let solicitacao = {}

    this.myForm.value.produto.forEach(produto => {
      sugestoes.push({
        produto : {
          id: produto.id,
        },
        quantidade : parseInt(produto.qtd),
      })
    })
    
    solicitacao = {
      piscina: {
        id: this.piscinaSelecionada.id,
      },
      sugestoes : sugestoes,
    }

    this.solicitacao = solicitacao

    this.realizarUpdate()
  }

  private async realizarUpdate(): Promise<any> {
    return await this.http.post('http://cleanpoolbackend.clicksistemas.com.br/sugestaocliente/',
      JSON.stringify(this.solicitacao), { headers: header })
      .subscribe(
        (response: Response) => {
          console.log(response)
          let alert = this.alertCtrl.create(
            {
              message: 'Logo entraremos em contato para confirmar a sua solicitação e enviaremos os seus produtos',
              buttons: ['OK']
            }
          )
          alert.present()
          this.navCtrl.setRoot('HomeClientePage');
        },
        (error: Error) => {
          console.log(error)
        }
      )
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Servico2Page } from './servico2';

@NgModule({
  declarations: [
    Servico2Page,
  ],
  imports: [
    IonicPageModule.forChild(Servico2Page),
  ],
})
export class Servico2PageModule {}

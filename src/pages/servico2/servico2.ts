import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { Camera, CameraOptions } from '@ionic-native/camera'
import { Http } from '@angular/http'
import header from './../../utils/headers'
import moment from 'moment'
import { BaseDadosOffProvider } from '../../providers/base-dados-off/base-dados-off';
import { Network } from '@ionic-native/network';

@IonicPage()
@Component({
  selector: 'page-servico2',
  templateUrl: 'servico2.html',
})
export class Servico2Page {

  public myForm: FormGroup;
  servicoChecked: any[];

  private listaIdServicos = []
  private API_URL = 'http://cleanpoolbackend.clicksistemas.com.br'
  protected horaFinal: string
  protected listaServicos: Array<any> = []
  protected dados: Array<any> = []
  protected dadosTratamento: any
  protected dadosPiscineiro: any
  private dadosServicosAgenda: Array<any> = []
  private agendaSelecionada: any
  private idAgenda: string
  private foto: any
  private horaInicio: string
  public dadosUsuario: any
  public conexao: String = "Online"

  public servicosAgendas: Array<any>=[]

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController,
    public menuCtrl: MenuController, public http: Http, private fb: FormBuilder,
    private camera: Camera, private toastCtrl: ToastController, private toast: ToastController,
    public baseDadosOffProvider: BaseDadosOffProvider, public network: Network, private alertCtrl: AlertController) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(false, 'piscineiro');

    this.servicoChecked = [];
  }

  async ngOnInit(): Promise<any> {
    this.network.onDisconnect().subscribe(() => {
      this.conexao = "Offline"
    }, error => console.log(error));

    this.network.onConnect().subscribe(() => {
      this.conexao = "Online"
    }, error => console.log(error));

    if (this.conexao == "Online") {
      this.modoOnline()
    }

    this.myForm = this.fb.group({
      servico: this.fb.array([]),
      servico2: this.fb.array([]),
      observacao: ['', Validators.compose([
        Validators.required
      ])],
    });

    let jsonTarefa3 = window.localStorage.getItem('agendaSelecionada')
    this.agendaSelecionada = JSON.parse(jsonTarefa3)

    this.idAgenda = this.agendaSelecionada.idAgenda

    let jsonTarefa = window.localStorage.getItem('dadosTratamento')
    this.dadosTratamento = JSON.parse(jsonTarefa)

    this.horaInicio = moment(this.dadosTratamento.dataInicio).format('HH:mm')

    let jsonTarefa2 = window.localStorage.getItem('dadosPiscineiro')
    this.dadosPiscineiro = JSON.parse(jsonTarefa2)

    let jsonTarefa1 = window.localStorage.getItem('dadosUsuario')
    this.dadosUsuario = JSON.parse(jsonTarefa1)

  }

  modoOnline(){
    let jsonTarefa3 = window.localStorage.getItem('agendaSelecionada')
    this.agendaSelecionada = JSON.parse(jsonTarefa3)

    this.idAgenda = this.agendaSelecionada.idAgenda

    this.consultaDadosServicosAgenda()
  }
  modoOffline(){
    this.conexao = "Offline"
    this.baseDadosOffProvider.getAllServicosAgendas()
      .then((result: Array<any>) => {
        this.servicosAgendas = result;

        this.servicosAgendas =  this.servicosAgendas.filter(servicoAgenda => servicoAgenda.idAgenda == this.idAgenda)

        this.montaListaServicos(this.servicosAgendas)
      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao carregar os Servicos Agendas.', duration: 3000, position: 'botton' }).present();
      });
  }

  private async consultaDadosServicosAgenda(): Promise<any> {
    await this.http.get(this.API_URL + '/query/agendaServicoFindByAgendaId?entityId=' + this.idAgenda,
      { headers: header })
      .subscribe(async (response: any) => {
        this.dadosServicosAgenda = await JSON.parse(response._body).data
        console.log(this.dadosServicosAgenda);

        this.montaListaServicos(this.dadosServicosAgenda)
      }, async (error: any) => {
        if (error.status === 0) {
          this.modoOffline()
        }
      })
  }

  private montaListaServicos(lista: Array<any>): void {
    for (let i = 0; i < lista.length; i++) {
      this.listaServicos.unshift({
        id: lista[i].id,
        nome: lista[i].nome,
        numero: i
      })
    }


  }

  onChange(servico: string, isChecked: boolean) {
    if (isChecked) {
      this.listaIdServicos.push(servico)
    }
  }

  salvarLocal() {

    let servicosEncontrados = []

    this.horaFinal = moment().format('HH:mm')

    this.listaServicos.forEach(servico => {
      if (this.listaIdServicos.find(id => id === servico.id) !== undefined) {
        servicosEncontrados.push({
          servico: {
            id: servico.id
          },
          status: 'ATENDIDO'
        })
      } else {
        servicosEncontrados.push({
          servico: {
            id: servico.id
          },
          status: 'NAOATENDIDO'
        })
      }
    })

    let servico = {
      horaInicio: this.dadosTratamento.horaInicio,
      horaFinal: this.horaFinal,
      observacao: this.myForm.value.observacao,
      cloroAdicionado: this.myForm.value.cloroAdicionado,
      servicos: servicosEncontrados,
    }

    let dadosServico = JSON.stringify(servico);

    localStorage.setItem('dadosServico', dadosServico)
  }

  goToOtherPageFoto() {
    if (this.conexao == "Online") {
      this.salvarLocal()
      this.navCtrl.push('AddFotoPage');
    }else{
      this.salvarLocal()
      let alert = this.alertCtrl.create(
        {
          message: 'No momento você não está conectado a internet, você poderá enviar as fotos posteriormente.',
          buttons: ['OK']
        }
      )
      alert.present()
      let quantidade = 0

      let quantidadeFotos = JSON.stringify(quantidade);

      localStorage.setItem('quantidadeFotos', quantidadeFotos)
      this.navCtrl.push('FinalizarAtendimentoPage');
    }
  }

  goToOtherPageFinalizar() {
    this.salvarLocal()
  }

  goToOtherPageSugerirProdutos() {
    this.salvarLocal()
    this.navCtrl.push('SugerirProdutosPage');
  }

}

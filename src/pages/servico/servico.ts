import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import moment from 'moment'
import { Http } from '@angular/http'
import header from './../../utils/headers'

@IonicPage()
@Component({
  selector: 'page-servico',
  templateUrl: 'servico.html',
})
export class ServicoPage {

  private dadosServico: any
  protected listaAlcalinidade: Array<any>
  protected listaPh: Array<any>
  private alcalinidadeEscolhida: number
  private durezaCalcicaEscolhida: number
  private phEscolhida: number
  private cloroEscolhido: number
  private acidoEscolhido: number
  private tamanhoPiscina: number
  protected gramasAdicionar: number
  protected gramas: number
  protected gramasAlcalinidade: number
  protected gramasPh: number
  protected gramasCloro: number
  protected gramasDc: number
  protected gramasAcido: number
  private msgAlcalinidade: string
  private msgPh: string
  private msgCloro: string
  private msgAcido: string
  private msgDurezaCalcica: string
  private dataInicio: string
  private horaInicio: string
  private data: Date
  private dadosPiscineiro: any
  private dadosPiscina: any
  private agenda: any
  public dadosUsuario: any
  private idPiscina: string
  private dosagemAlcalinidade : string
  private dosagemPh : string
  private dosagemCloro : string
  private dosagemAcido : string
  private dosagemDurezaCalcica : string

  async ngOnInit(): Promise<any> {

    let jsonTarefa1 = window.localStorage.getItem('dadosUsuario')
    this.dadosUsuario = JSON.parse(jsonTarefa1)

    let jsonTarefa = window.localStorage.getItem('agendaSelecionada')

    this.agenda = JSON.parse(jsonTarefa)

    let jsonTarefa2 = window.localStorage.getItem('dadosPiscina')

    this.dadosPiscina = JSON.parse(jsonTarefa2)

    this.tamanhoPiscina = this.dadosPiscina[0].volumeagua
    
    this.idPiscina = this.agenda.idPiscina
    
    this.dataInicio = new Date ().toISOString()
    
    this.horaInicio = moment(this.dataInicio).format('HH:mm')

  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, public alertCtrl: AlertController, public http: Http) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(false, 'piscineiro');
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Parâmetros Inconpleto',
      subTitle: 'Por favor informar todos os parâmetros',
      buttons: ['OK']
    });
    alert.present();
  }

  goToOtherPage() {
    if (this.alcalinidadeEscolhida) {
      if(this,this.alcalinidadeEscolhida == 10){
        this.gramasAlcalinidade = 180 * (this.tamanhoPiscina/1000)
        
      }else if((this,this.alcalinidadeEscolhida == 20) ||  (this,this.alcalinidadeEscolhida == 200)){
        this.gramasAlcalinidade = 160 * (this.tamanhoPiscina/1000)

      }else if((this,this.alcalinidadeEscolhida == 30) ||  (this,this.alcalinidadeEscolhida == 190)){
        this.gramasAlcalinidade = 140 * (this.tamanhoPiscina/1000)

      }else if((this,this.alcalinidadeEscolhida == 40) ||  (this,this.alcalinidadeEscolhida == 180)){
        this.gramasAlcalinidade = 120 * (this.tamanhoPiscina/1000)

      }else if((this,this.alcalinidadeEscolhida == 50) ||  (this,this.alcalinidadeEscolhida == 170)){
        this.gramasAlcalinidade = 100 * (this.tamanhoPiscina/1000)

      }else if((this,this.alcalinidadeEscolhida == 60) ||  (this,this.alcalinidadeEscolhida == 160)){
        this.gramasAlcalinidade = 80 * (this.tamanhoPiscina/1000)

      }else if((this,this.alcalinidadeEscolhida == 70) ||  (this,this.alcalinidadeEscolhida == 150)){
        this.gramasAlcalinidade = 60 * (this.tamanhoPiscina/1000)

      }else if((this,this.alcalinidadeEscolhida == 80) ||  (this,this.alcalinidadeEscolhida == 140)){
        this.gramasAlcalinidade = 40 * (this.tamanhoPiscina/1000)

      }else if(this,this.alcalinidadeEscolhida == 130){
        this.gramasAlcalinidade = 20 * (this.tamanhoPiscina/1000)

      }

      if ((this.alcalinidadeEscolhida >= 10) && (this.alcalinidadeEscolhida <= 70) ) {
        if(this.gramasAlcalinidade > 1000){
          let x = this.gramasAlcalinidade / 1000
          let kilo = x.toFixed(0)
          
          this.msgAlcalinidade = 'O valor de ' + this.alcalinidadeEscolhida + ' ppm não está bom. Alcalinidade total entre 80 e 120 ppm contribui para a estabilidade da água, garante máxima eficiência do cloro livre.'
          this.dosagemAlcalinidade = 'Adicionar ' + kilo + 'Kg de elevador de alcalinidade.'
        }else{
          let x = this.gramasAlcalinidade.toFixed(0)

          this.msgAlcalinidade = 'O valor de ' + this.alcalinidadeEscolhida + ' ppm não está bom. Alcalinidade total entre 80 e 120 ppm contribui para a estabilidade da água, garante máxima eficiência do cloro livre.'
          this.dosagemAlcalinidade = 'Adicionar ' + x + 'g de elevador de alcalinidade.'
        }
      } else if (this.alcalinidadeEscolhida >= 130) {
        if(this.gramasAlcalinidade > 1000){
          let x = this.gramasAlcalinidade / 1000
          let kilo = x.toFixed(0)
  
          this.msgAlcalinidade = 'O valor de ' + this.alcalinidadeEscolhida + ' ppm não está bom. Alcalinidade total entre 80 e 120 ppm contribui para a estabilidade da água, garante máxima eficiência do cloro livre.'
          this.dosagemAlcalinidade = 'Adicionar ' + kilo + 'l de redutor de alcalinidade.'
        }else{
          let x = this.gramasAlcalinidade.toFixed(0)

          this.msgAlcalinidade = 'O valor de ' + this.alcalinidadeEscolhida + ' ppm não está bom. Alcalinidade total entre 80 e 120 ppm contribui para a estabilidade da água, garante máxima eficiência do cloro livre.'  
          this.dosagemAlcalinidade = 'Adicionar ' + x + 'mls de redutor de alcalinidade.'
        }
      }else{
        this.msgAlcalinidade = 'O valor de ' + this.alcalinidadeEscolhida + ' ppm está bom. Alcalinidade total entre 80 e 120 ppm contribui para a estabilidade da água, garante máxima eficiência do cloro livre.'
        this.dosagemAlcalinidade = 'Alcalinidade está estavel'
      }
      
    }

    if (this.phEscolhida) {
      if(this.phEscolhida == 6.7 || this.phEscolhida == 8.2){
        this.gramasPh = 20 * (this.tamanhoPiscina/1000)

      }else if(this.phEscolhida == 7.8 || this.phEscolhida == 7.2){
        this.gramasPh = 10 * (this.tamanhoPiscina/1000)

      }

      if ((this.phEscolhida >= 6.7) && (this.phEscolhida <= 7.2) ) {
        if(this.gramasPh > 999){
          let x = this.gramasPh / 1000
          let kilo = x.toFixed(0)

          this.msgPh = 'O valor de ' + this.phEscolhida + ' não está bom. pH: entre 7,2 e 7,8 garante maxima eficiência do cloro livre, previne corrosão dos equipamentos e acessórios, além de propocionar água pura e suave para pele, olhos e cabelos.'
          this.dosagemPh = 'Adicionar ' + kilo + 'Kg de Elevador de PH.' 
        }else{
          let x = this.gramasPh.toFixed(0)

          this.msgPh = 'O valor de ' + this.phEscolhida + ' não está bom. pH: entre 7,2 e 7,8 garante maxima eficiência do cloro livre, previne corrosão dos equipamentos e acessórios, além de propocionar água pura e suave para pele, olhos e cabelos.'
          this.dosagemPh = 'Adicionar ' + x + 'g de Elevador de PH.'
        }
      } else if (this.phEscolhida >= 7.8) {
        if(this.gramasPh > 999){
          let x = this.gramasPh / 1000
          let kilo = x.toFixed(0)

          this.msgPh = 'O valor de ' + this.phEscolhida + ' não está bom. pH: entre 7,2 e 7,8 garante maxima eficiência do cloro livre, previne corrosão dos equipamentos e acessórios, além de propocionar água pura e suave para pele, olhos e cabelos.'
          this.dosagemPh = 'Adicionar ' + kilo + 'L de Redutor de PH.' 
        }else{
          let x = this.gramasPh.toFixed(0)

          this.msgPh = 'O valor de ' + this.phEscolhida + ' não está bom. pH: entre 7,2 e 7,8 garante maxima eficiência do cloro livre, previne corrosão dos equipamentos e acessórios, além de propocionar água pura e suave para pele, olhos e cabelos.'
          this.dosagemPh = 'Adicionar ' + x + 'ml de Redutor de PH.'
        } 
      }else{
        this.msgPh = 'O valor de ' + this.phEscolhida + ' está bom. pH: entre 7,2 e 7,8 garante maxima eficiência do cloro livre, previne corrosão dos equipamentos e acessórios, além de propocionar água pura e suave para pele, olhos e cabelos.'
        this.dosagemPh = 'PH Estável'
      }
    }

    if (this.cloroEscolhido) {
      if (this.cloroEscolhido == 0 || this.cloroEscolhido == 0.5 || this.cloroEscolhido == 1) {
        this.gramasCloro = 14 * (this.tamanhoPiscina/1000)
        
        if(this.gramasCloro > 999){
          let x = this.gramasCloro / 1000
          let kilo = x.toFixed(0)

          this.msgCloro = 'O valor de ' + this.cloroEscolhido + ' não está bom. Cloro livre: entre 2 e 3 ppm garante a pureza da água ao eliminar os microbios.'
          this.dosagemCloro = 'Adicionar ' + kilo + 'Kg de Cloro.'
        }else{
          let x = this.gramasCloro.toFixed(0)

          this.msgCloro = 'O valor de ' + this.cloroEscolhido + ' não está bom. Cloro livre: entre 2 e 3 ppm garante a pureza da água ao eliminar os microbios.'
          this.dosagemCloro = 'Adicionar ' + x + 'g de Cloro.'
        } 
      } else if (this.cloroEscolhido == 1.5){
        this.gramasCloro = 7 * (this.tamanhoPiscina/1000)
        if(this.gramasCloro > 999){
          let x = this.gramasCloro / 1000
          let kilo = x.toFixed(0)

          this.msgCloro = 'O valor de ' + this.cloroEscolhido + ' não está bom. Cloro livre: entre 2 e 3 ppm garante a pureza da água ao eliminar os microbios.'
          this.dosagemCloro = 'Adicionar ' + kilo + 'Kg de Cloro.'
        }else{
          let x = this.gramasCloro.toFixed(0)

          this.msgCloro = 'O valor de ' + this.cloroEscolhido + ' não está bom. Cloro livre: entre 2 e 3 ppm garante a pureza da água ao eliminar os microbios.'
          this.dosagemCloro = 'Adicionar ' + x + 'g de Cloro.'
        } 
      }else if (this.cloroEscolhido == 3 || this.cloroEscolhido == 2){
        this.msgCloro = 'O valor de ' + this.cloroEscolhido + ' está bom. Cloro livre: entre 2 e 3 ppm garante a pureza da água ao eliminar os microbios.'
        this.dosagemCloro = 'O nivel de cloro está estável.'
      }else if (this.cloroEscolhido == 5){

        this.msgCloro = 'O valor de ' + this.cloroEscolhido + ' não está bom. Cloro livre: entre 2 e 3 ppm garante a pureza da água ao eliminar os microbios.'
        this.dosagemCloro = 'Aguardar baixar o cloro.'
      }
    }

    if (this.durezaCalcicaEscolhida) {
      if(this.durezaCalcicaEscolhida == 100){
        this.dosagemDurezaCalcica = 'Dureza Cálcica estável'
      }else if(this.durezaCalcicaEscolhida == 250){

        let x = this.tamanhoPiscina * 0.3
        let troca = x.toFixed(0)

        this.dosagemDurezaCalcica = 'Retroque 30% (' + troca +'l) de água da piscina, e aguarde o cálcio baixar.'
      }
      else {
        let multiplicador = ((100 - this.durezaCalcicaEscolhida)/10)

        this.gramasDc = (15 * multiplicador)*(this.tamanhoPiscina/1000)
        
        if(this.gramasDc > 1000){
          let x = this.gramasDc / 1000
          let kilo = x.toFixed(0)

          this.dosagemDurezaCalcica = 'Adicionar ' + kilo + 'Kg de elevador hidrocalcio.'
        }else{
          let x = this.gramasDc.toFixed(0)

          this.dosagemDurezaCalcica = 'Adicionar ' + x + 'g de elevador hidrocalcio.'
        }
      } 
    }

    if (this.acidoEscolhido) {
      if(this.acidoEscolhido == 0 || this.acidoEscolhido == 50 ){
        this.msgAcido = 'O valor de ' + this.acidoEscolhido + ' está bom. Ácido Cianúrico: acima de 50 ppm prejudica a eficiência da purificação do cloro livre.'
        this.dosagemAcido = 'O nivel de Ácido Cianúrico está estavel'
      }else{
        let x = this.tamanhoPiscina * 0.3
        let troca = x.toFixed(0)

        this.msgAcido = 'O valor de ' + this.acidoEscolhido + ' não está bom. Ácido Cianúrico: acima de 50 ppm prejudica a eficiência da purificação do cloro livre.'
        this.dosagemAcido = 'Retroque 30% (' + troca +'l) de água da piscina, ou suspender o uso do cloro estabilizado.'
      
      }
    }
    
    if(this.alcalinidadeEscolhida && this.phEscolhida && this.acidoEscolhido && this.cloroEscolhido && this.durezaCalcicaEscolhida){
      
      this.dadosServico = {
        dataInicio: this.dataInicio,
        dosagemAlcalinidade : this.dosagemAlcalinidade,
        msgAlcalinidade : this.msgAlcalinidade,
        dosagemPh: this.dosagemPh,
        msgPh : this.msgPh,
        dosagemCloro: this.dosagemCloro,
        msgCloro : this.msgCloro,
        dosagemAcido: this.dosagemAcido,
        msgAcido : this.msgAcido,
        dosagemDurezaCalcica: this.dosagemDurezaCalcica,
        msgDurezaCalcica : this.msgDurezaCalcica
      }
  
      var jsonAux = JSON.stringify(this.dadosServico);
  
      localStorage.setItem('dadosTratamento', jsonAux)  
      
      this.navCtrl.push('DadosServicoPage');
    }else{
      this.showAlert() 
    }

  }

}

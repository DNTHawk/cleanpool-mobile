import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import header from './../../utils/headers'
import { verify } from 'jsonwebtoken'

@IonicPage()
@Component({
  selector: 'page-selecionar-piscina',
  templateUrl: 'selecionar-piscina.html',
})
export class SelecionarPiscinaPage {

  protected listaPiscinas: string[]

  protected ngOnInit(): void {
    let tokenDecodificado: any = verify(localStorage.getItem('token'), 'keyClickCleanPoollllLoginXXX')
    this.consultaTodosDadosCliente(tokenDecodificado.pessoaId)
    this.consultaPiscinaCliente(tokenDecodificado.pessoaId)
    // this.doisClicksSair()
  }

  constructor(public http: Http, public navCtrl: NavController) {
  }

  private async consultaTodosDadosCliente(pessoaId: string): Promise<any> {
    let dadosClienteJSON: any
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/fisicaFindById?entityId=${pessoaId}`,
      { headers: header })
      .subscribe(async (resposta: any) => {
        dadosClienteJSON = JSON.parse(resposta._body)
        await localStorage.setItem('dadosCliente', JSON.stringify(dadosClienteJSON.data[0]))
      })
    // this.nomeCliente = JSON.parse(localStorage.getItem('dadosCliente')).nome
  }

  private consultaPiscinaCliente(pessoaId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/piscinaFindByPessoaId?entityId=${pessoaId}`,
        { headers: header })
        .subscribe((resposta: any) => {
          resolve(resposta)
        }, error => reject(error.json()))
    })
      .then((resposta: any) => {
        this.listaPiscinas = JSON.parse(resposta._body).data
      })
      .catch((err: any) => console.error(`ERRO: ${err}`))
  }

  ionViewDidLoad() {

  }

  goToPageProduto(objSelecionado) {

    var jsonAux = JSON.stringify(objSelecionado);

    localStorage.setItem('piscinaSelecionada', jsonAux)

    this.navCtrl.push('FazerPedidoPage')
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelecionarPiscinaPage } from './selecionar-piscina';

@NgModule({
  declarations: [
    SelecionarPiscinaPage,
  ],
  imports: [
    IonicPageModule.forChild(SelecionarPiscinaPage),
  ],
})
export class SelecionarPiscinaPageModule {}

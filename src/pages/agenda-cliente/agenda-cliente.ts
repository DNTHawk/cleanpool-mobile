import { Http } from '@angular/http';
import { Component } from '@angular/core'
import { IonicPage, NavController } from 'ionic-angular'
import headers from './../../utils/headers'
import moment from 'moment'
import { ToastController } from 'ionic-angular'

@IonicPage()
@Component({
  selector: 'page-agenda-cliente',
  templateUrl: 'agenda-cliente.html',
})

export class AgendaClientePage {

  private listaAgendasAux = []
  private listaAgendas = []
  private exibeAberto = true
  protected mensagemToogle: string

  private event: any = {
    startDate: '',
    endDate: ''
  }

  constructor(public navCtrl: NavController, private http: Http, private toastController: ToastController) { }

  protected ngOnInit(): void {
    this.setaDataInicialFiltro()
    this.consultaAgendaCliente()
  }

  private setaDataInicialFiltro(): void {
    this.event = {
      startDate: moment().format('YYYY-MM-DD'),
      endDate: moment().add(1, 'month').format('YYYY-MM-DD'),
    }
  }

  private presentToast(): void {
    let toast = this.toastController.create({
      message: 'A data inicial deve ser menor que a final!',
      duration: 3500,
      position: 'bottom'
    })
    toast.present()
  }

  private async consultaAgendaCliente(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/agendaFindByClienteLogin`,
      { headers: headers })
      .subscribe(async (response: any) => {
        const aux = await JSON.parse(response._body).data
        this.montaListaAgenda(aux)
      }, (error: any) => {
        console.error('Erro ao consultar agenda do cliente. Erro:', error)
    })
  }

  private montaListaAgenda(lista: Array<any>): void {
    for (let i = 0; i < lista.length; i++) {
      this.listaAgendas.unshift({
        nomePiscineiro: lista[i].nomepiscineiro,
        dataAtendimento: moment(lista[i].dataagendamento).format('YYYY-MM-DD'),
        horarioAtendimento: moment(lista[i].dataagendamento).format('H:mm'),
        dataFim: lista[i].datatermino ? moment(lista[i].datatermino).format('YYYY-MM-DD H:mm') : null,
        dataInicio: lista[i].datainicio ? moment(lista[i].datainicio).format('YYYY-MM-DD H:mm') : null
      })
    }
    this.filtraPorData()

    this.manipulaParaListaAgenda(this.exibeAberto)
  }

  private filtraPorData(): void {
    this.listaAgendasAux = []
    this.listaAgendas.forEach(agenda => {
      moment(agenda.dataAtendimento).add(1, 'day').isBetween(this.event.startDate, this.event.endDate)
        ? this.listaAgendasAux.unshift({
          nomePiscineiro: agenda.nomePiscineiro,
          dataAtendimento: moment(agenda.dataAtendimento).format('DD/MM/YYYY'),
          horarioAtendimento: agenda.horarioAtendimento,
          dataFim: agenda.dataFim ? moment(agenda.dataFim).format('DD/MM/YYYY H:mm') : null,
          dataInicio: agenda.dataInicio ? moment(agenda.dataInicio).format('DD/MM/YYYY H:mm') : null
        })
        : null
      })
  }

  protected manipulaParaListaAgenda(exibeAberto: boolean): void {
    exibeAberto ? this.agendaAberta() : this.agendaEncerrados()
  }

  private agendaAberta(): void {
    this.mensagemToogle = 'Consultar Serviços Encerrados'
    this.listaAgendas = []
    for (let i = 0; i < this.listaAgendasAux.length; i++) {
      if (!this.listaAgendasAux[i].dataFim) {
        this.listaAgendas.unshift({
          nomePiscineiro: this.listaAgendasAux[i].nomePiscineiro,
          dataAtendimento: this.listaAgendasAux[i].dataAtendimento,
          horarioAtendimento: this.listaAgendasAux[i].horarioAtendimento,
          dataFim: this.listaAgendasAux[i].dataFim,
          dataInicio: this.listaAgendasAux[i].dataInicio,
          situacao: 'Aberto'
        })
      }
    }
  }

  private agendaEncerrados(): void {
    this.mensagemToogle = 'Consultar Serviços Abertos'
    this.listaAgendas = []
    for (let i = 0; i < this.listaAgendasAux.length; i++) {
      if (this.listaAgendasAux[i].dataFim) {
        this.listaAgendas.unshift({
          nomePiscineiro: this.listaAgendasAux[i].nomePiscineiro,
          dataAtendimento: this.listaAgendasAux[i].dataAtendimento,
          horarioAtendimento: this.listaAgendasAux[i].horarioAtendimento,
          dataFim: this.listaAgendasAux[i].dataFim,
          dataInicio: this.listaAgendasAux[i].dataInicio,
          situacao: 'Encerrado'
        })
      }
    }
  }

  logOut() {
    localStorage.clear()

    this.navCtrl.setRoot('TelaLoginPage');
  }

}

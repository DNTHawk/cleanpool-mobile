import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { verify } from 'jsonwebtoken'
import { Http } from '@angular/http';
import header from './../../utils/headers'
// import { Platform, ToastController } from 'ionic-angular'

@IonicPage()
@Component({
  selector: 'page-home-cliente',
  templateUrl: 'home-cliente.html',
})
export class HomeClientePage {

  public nomeCliente: string
  protected listaPiscinas: string[]
  // private counter: number = 0
  private API_URL = 'http://cleanpoolbackend.clicksistemas.com.br'
  private tipos : Array<string> = []
  protected data: string
  protected dadosPiscineiro: Array<any> = []
  protected dadosUsuario: any = []

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public http: Http) {
    let jsonTarefa = window.localStorage.getItem('dadosUsuario')
    this.dadosUsuario = JSON.parse(jsonTarefa)
    this.tipos = this.dadosUsuario.tipos

    if(this.tipos.length > 1){
      this.menuCtrl.enable(false, 'cliente');
      this.menuCtrl.enable(true, 'clientePiscineiro');
    }else{
      this.menuCtrl.enable(true, 'cliente');
      this.menuCtrl.enable(false, 'clientePiscineiro');
    }
  }
  
  protected ngOnInit(): void {

    let tokenDecodificado: any = verify(localStorage.getItem('token'), 'keyClickCleanPoollllLoginXXX')
    this.consultaTodosDadosCliente(tokenDecodificado.pessoaId)
    this.consultaPiscinaCliente(tokenDecodificado.pessoaId)
    // this.doisClicksSair()
  }

  private async consultaTodosDadosCliente(pessoaId: string): Promise<any> {
    let dadosClienteJSON: any
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/fisicaFindById?entityId=${pessoaId}`,
      { headers: header })
      .subscribe(async (resposta: any) => {
        dadosClienteJSON = JSON.parse(resposta._body)
        await localStorage.setItem('dadosCliente', JSON.stringify(dadosClienteJSON.data[0]))
      })
    // this.nomeCliente = JSON.parse(localStorage.getItem('dadosCliente')).nome
  }

  private consultaPiscinaCliente(pessoaId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/piscinaFindByPessoaId?entityId=${pessoaId}`,
        { headers: header })
        .subscribe((resposta: any) => {
          resolve(resposta)
        }, error => reject(error.json()))
    })
      .then((resposta: any) => {
        this.listaPiscinas = JSON.parse(resposta._body).data
      })
      .catch((err: any) => console.error(`ERRO: ${err}`))
  }

  protected goToPagePedido(): void {
    this.navCtrl.push('SelecionarPiscinaPage')
  }

  protected goToPageConteudo(): void {
    this.navCtrl.push('ConteudoPage')
  }

  protected goToPageAgenda(): void {
    this.navCtrl.push('AgendaClientePage')
  }

  protected goToPageServico(objSelecionado){
    var jsonAux = JSON.stringify(objSelecionado);

    localStorage.setItem('piscinaSelecionada', jsonAux)

    this.navCtrl.push('TelaSolicitacaoServicoPage')
  }

  logOut(){
    localStorage.clear()

    this.navCtrl.setRoot('TelaLoginPage');
  }

}

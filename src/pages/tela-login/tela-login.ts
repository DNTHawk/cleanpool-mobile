import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as jwt from 'jsonwebtoken';
import { Http, Response} from '@angular/http';
import { Network } from '@ionic-native/network';
import { DatabaseProvider } from '../../providers/database/database'
import { LoginProvider } from '../../providers/login/login';

@IonicPage()
@Component({
  selector: 'page-tela-login',
  templateUrl: 'tela-login.html',
})
export class TelaLoginPage {

  activeMenu: string
  public form: FormGroup
  protected dialogErro: boolean = false
  isTextFieldType: boolean;
  public conexao: String = "Online"
  public logins: any;
  public dadosSalvar: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private fb: FormBuilder,
    public menuCtrl: MenuController, public http: Http, public alertCtrl: AlertController,
    public network: Network, public dbProvider: DatabaseProvider,
    public loginProvider: LoginProvider, private toast: ToastController) {

    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(false, 'piscineiro');

    this.form = this.fb.group({
      login: ['', Validators.compose([
        Validators.minLength(1),
        Validators.maxLength(20),
        Validators.required
      ])],
      senha: ['', Validators.compose([
        Validators.minLength(1),
        Validators.maxLength(20),
        Validators.required
      ])],
    })
  }

  async ngOnInit(): Promise<any> {
    this.network.onDisconnect().subscribe(() => {
      this.conexao = "Offline"
    }, error => console.log(error));

    this.network.onConnect().subscribe(() => {
      this.conexao = "Online"
    }, error => console.log(error));

    this.loginProvider.getAll()
    .then((result: any[]) => {
      this.logins = result;
    })
    .catch(() => {
      this.toast.create({ message: 'Verificar conexão.', duration: 3000, position: 'botton' }).present();
    });

  }

  async autenticarAcesso() {
    if (this.conexao == 'Online') {

      let token: any
      let dadosLogin = {
        login: this.form.controls.login.value,
        senha: this.form.controls.senha.value
      }
      

      this.http.post('http://cleanpoolbackend.clicksistemas.com.br/login', dadosLogin)
      .subscribe((resultado: Response) => {
        let resposta = resultado.headers.values()
        token = resposta[1]
        
        this.guardaToken(token[0].replace('Bearer ', ''))
      }, async (error: any) => {
        if (error.status === 0) {
          this.modoOffline();
        } else if (error.status === 401) {
          this.dialogErro = true

          this.form.reset()

          setTimeout(() => {
            this.dialogErro = false
          }, 3000)
        }


      })
    }

    if (this.conexao == 'Offline') {
      this.modoOffline();
    }
  }

  async modoOffline(){
    let dadosLogin = {
      login: this.form.controls.login.value,
      senha: this.form.controls.senha.value
    }

    if (dadosLogin.login == this.logins[0].login) {
      if (dadosLogin.senha == this.logins[0].password) {

        let jsonAux = JSON.stringify(this.logins[0]);

        localStorage.setItem('dadosUsuario', jsonAux)

        if (this.logins[0].tipo1 == "CLIENTE" && this.logins[0].tipo2 == "PISCINEIRO") {
          this.navCtrl.setRoot('TelaPerfilPage')
        } else if (this.logins[0].tipo2 == "CLIENTE" && this.logins[0].tipo1 == "PISCINEIRO") {
          this.navCtrl.setRoot('TelaPerfilPage')
        } else if (this.logins[0].tipo1 == "FUNCIONARIO" && this.logins[0].tipo2 == "PISCINEIRO"){
          this.navCtrl.setRoot('LoadingsPage')
        } else if (this.logins[0].tipo1 == "PISCINEIRO" && this.logins[0].tipo2 == "FUNCIONARIO"){
          this.navCtrl.setRoot('LoadingsPage')
        } else if (this.logins[0].tipo1 == "CLIENTE" && this.logins[0].tipo2 == "PISCINEIRO"){
          this.navCtrl.setRoot('AvaliacaoPage')
        } else if (this.logins[0].tipo1 == "PISCINEIRO" && this.logins[0].tipo2 == "CLIENTE"){
          this.navCtrl.setRoot('AvaliacaoPage')
        } else if (this.logins[0].tipo1 == "CLIENTE"){
          this.navCtrl.setRoot('AvaliacaoPage')
        } else if (this.logins[0].tipo1 == "PISCINEIRO"){
          this.navCtrl.setRoot('LoadingsPage')
        }else {
          this.dialogErro = true
        
          this.form.reset()

          setTimeout(() => {
            this.dialogErro = false
          }, 3000)
        }

      } else {
        this.dialogErro = true

        this.form.reset()

        setTimeout(() => {
          this.dialogErro = false
        }, 3000)
      }
    } else {
      this.dialogErro = true

      this.form.reset()

      setTimeout(() => {
        this.dialogErro = false
      }, 3000)
    }

  }

  goToOtherPageCadastro() {
    this.navCtrl.push('TipoPessoaPage');
  }

  guardaToken(token: string) {
    jwt.verify(token, 'keyClickCleanPoollllLoginXXX', (err, payload: any) => {
      if (payload.tipos.length > 0) {
        console.log(token);
        

        let tipos = payload.tipos

        let piscineiro = 0
        let cliente = 0

        for (let i = 0; i < tipos.length; i++) {
          if (tipos[i] === "PISCINEIRO") {
            piscineiro = 1
          }

          if (tipos[i] === "CLIENTE") {
            cliente = 1
          }
        }

        localStorage.setItem('token', token)

        let dadosSalvar ={
          nome: payload.sub,
          login: this.form.controls.login.value,
          senha: this.form.controls.senha.value,
          tipo: payload.tipo,
          tipos: payload.tipos,
          idUser: payload.pessoaId
        }

        let jsonAux = JSON.stringify(dadosSalvar);

        localStorage.setItem('dadosUsuario', jsonAux)

        this.dbProvider.createDatabase(dadosSalvar)
          .then(()=>{
            if (piscineiro === 1 && cliente === 1) {
              this.navCtrl.setRoot('TelaPerfilPage')
            } else if (piscineiro === 1 && cliente === 0) {
              this.navCtrl.setRoot('AgendaPage')
            } else if (piscineiro === 0 && cliente === 1) {
              this.navCtrl.setRoot('AvaliacaoPage')
            }
          })
          .catch(() => {
            const alert = this.alertCtrl.create({
              title: 'Erro',
              subTitle: 'Não foi possivel criar o banco de dados',
              buttons: ['OK']
            });
            alert.present();
          })

      } else {
        alert('Este usuário não foi vinculado a um tipo, como cliente, piscineiro ou funcionário.')
      }
    })
  }

  redefinirSenha() {
    this.navCtrl.push('TelaSolicitacaoTrocaSenhaPage');
  }

  togglePasswordFieldType() {
    this.isTextFieldType = !this.isTextFieldType;
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { Http, Response } from '@angular/http'
import header from './../../utils/headers'
import moment from 'moment'
import { LoginProvider } from '../../providers/login/login'
import { BaseDadosOffProvider } from '../../providers/base-dados-off/base-dados-off'
import { Network } from '@ionic-native/network';

@IonicPage()
@Component({
  selector: 'page-agenda',
  templateUrl: 'agenda.html',
})
export class AgendaPage {

  private API_URL = 'http://cleanpoolbackend.clicksistemas.com.br'
  private listaAgendasAux: Array<any> = []
  private listaAgendas: Array<any> = []
  private listaAgendasState: Array<any> = []
  private tipos: Array<string> = []
  protected data: string
  protected mensagemToogle: string
  protected mensagemToogleAgenda: string = "Consultar todas as agendas"
  protected exibeAberto: boolean = true
  protected dadosPiscineiro: Array<any> = []
  protected dadosUsuario: any
  protected botaoPerfil: boolean = false
  protected situacao: string = "Aberto"
  protected status: string = "Dia"
  private idPiscineiro: string
  private tipoPiscineiro: string
  protected dataFiltro: string = moment().format('YYYY-MM-DD')

  public conexao: String = "Online"

  public logins: any[];
  public pessoas: Array<any> = [];
  public grupoServicos: Array<any> = [];
  public servicos: Array<any> = [];
  public grupoProdutos: Array<any> = [];
  public subGrupoProdutos: Array<any> = [];
  public produtos: Array<any> = [];
  public agendas: Array<any> = [];
  public piscinas: Array<any> = [];
  public servicosAgendas: Array<any>=[]

  public listaPessoas: Array<any> = []
  public listaSubGruposServicos: Array<any> = []
  public listaServicos: Array<any> = []
  public listaGrupoProdutos: Array<any> = []
  public listaSubGrupoProdutos: Array<any> = []
  public listaProdutos: Array<any> = []
  public listaPiscinas: Array<any> = []
  public listaServicosAgendas: Array<any> = []

  public agendaFinalizada: Array<any> = []
  public listaProdutosSugeridos: Array<any> = []
  public listaServicosAtendidos: Array<any> = []
  private agendasFinalizadas: any
  private produtosAgendas: Array<any> = [];

  constructor(public navCtrl: NavController,private toastCtrl: ToastController ,
    public navParams: NavParams, public menuCtrl: MenuController, public http: Http,
    public loginProvider: LoginProvider, private toast: ToastController,
    public baseDadosOffProvider: BaseDadosOffProvider, public network: Network) {
  }

  async ngOnInit(): Promise<any> {
    this.network.onDisconnect().subscribe(() => {
      this.conexao = "Offline"
    }, error => console.log(error));

    this.network.onConnect().subscribe(() => {
      this.conexao = "Online"
    }, error => console.log(error));

    if (this.conexao == "Online") {
      this.modoOnline()
    }else if (this.conexao == "Offline"){
      this.modoOffline()
    }

    let jsonTarefa = window.localStorage.getItem('dadosUsuario')
    this.dadosUsuario = JSON.parse(jsonTarefa)
    
    console.log(this.dadosUsuario);

    if (this.dadosUsuario.tipos[0] == "CLIENTE" && this.dadosUsuario.tipos[1] == "PISCINEIRO") {
      this.botaoPerfil = true
    } else if (this.dadosUsuario.tipos[0] == "CLIENTE" && this.dadosUsuario.tipos[1] == "PISCINEIRO") {
      this.botaoPerfil = true
    } else if (this.dadosUsuario.tipos[0] == "FUNCIONARIO" && this.dadosUsuario.tipos[1] == "PISCINEIRO"){
      this.botaoPerfil = false
    } else if (this.dadosUsuario.tipos[0] == "PISCINEIRO" && this.dadosUsuario.tipos[1] == "FUNCIONARIO"){
      this.botaoPerfil = false
    } else if (this.dadosUsuario.tipos[0] == "CLIENTE" && this.dadosUsuario.tipos[1] == "PISCINEIRO"){
      this.botaoPerfil = false
    } else if (this.dadosUsuario.tipos[0] == "PISCINEIRO" && this.dadosUsuario.tipos[1] == "CLIENTE"){
      this.botaoPerfil = false
    } else if (this.dadosUsuario.tipos[0] == "CLIENTE"){
      this.botaoPerfil = false
    } else if (this.dadosUsuario.tipos[0] == "PISCINEIRO"){
      this.botaoPerfil = false
    }
  }

  public async modoOnline(){
    // this.verificaAgendaPendente()

    this.consultaListaPiscinas()
    this.consultaListaPessoas()
    this.consultaListaSubGruposServicos()
    this.consultaListaServicos()
    this.consultaListaGrupoProdutos()
    this.consultaListaSubGrupoProdutos()
    this.consultaListaProdutos()
    this.consultaServicosAgendas()
    this.consultaListaAgendas()
  }

  public async modoOffline(){

    this.loginProvider.getAll()
      .then((result: any[]) => {
        this.logins = result;

      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao carregar os logins.', duration: 3000, position: 'botton' }).present();
      });

    this.baseDadosOffProvider.getAllAgendas()
      .then((result: Array<any>) => {
        this.agendas = result;

        if (this.situacao == "Aberto") {
          for (let i = 0; i < this.agendas.length; i++) {
            if (this.agendas[i].status == "NAOATENDIDO") {
              this.listaAgendas.unshift({
                nomecliente: this.agendas[i].nomecliente,
                datainicio: this.agendas[i].dataagendamento ? moment(this.agendas[i].dataagendamento).format('DD/MM/YYYY') : '(Não informado)',
                horainicio: this.agendas[i].dataagendamento ? moment(this.agendas[i].dataagendamento).format('HH:mm') : '(Não informado)',
                datatermino: this.agendas[i].datatermino || null,
                idCliente: this.agendas[i].idcliente,
                idPiscineiro: this.agendas[i].idpiscineiro,
                idAgenda: this.agendas[i].id,
                idPiscina: this.agendas[i].idpiscina,
                situacao: "Aberto"
              })
            }
          }
        }
        if (this.situacao == "Encerrado") {
          for (let i = 0; i < this.agendas.length; i++) {
            if (this.agendas[i].status == "ATENDIDO") {
              this.listaAgendas.unshift({
                nomecliente: this.agendas[i].nomecliente,
                datainicio: this.agendas[i].dataagendamento ? moment(this.agendas[i].dataagendamento).format('DD/MM/YYYY') : '(Não informado)',
                horainicio: this.agendas[i].dataagendamento ? moment(this.agendas[i].dataagendamento).format('HH:mm') : '(Não informado)',
                datatermino: this.agendas[i].datatermino || null,
                idCliente: this.agendas[i].idcliente,
                idPiscineiro: this.agendas[i].idpiscineiro,
                idAgenda: this.agendas[i].id,
                idPiscina: this.agendas[i].idpiscina,
                situacao: "Encerrado"
              })
            }
          }
        }
        this.listaAgendasAux = this.agendas

        this.listaAgendasState = this.listaAgendas

        this.manipulaParaListaAgenda(this.exibeAberto)

        this.filtrarData(this.dataFiltro)
      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao carregar os agendas.', duration: 3000, position: 'botton' }).present();
      });

  }

  public async consultaListaPiscinas(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/piscina/all`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaPiscinas = await JSON.parse(response._body)

        this.baseDadosOffProvider.insertPiscinas(this.listaPiscinas)

      }, async (error: any) => {
        if (error.status === 0) {
          this.modoOffline()
        }
      })
  }

  public async consultaListaPessoas(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/pessoaFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaPessoas = await JSON.parse(response._body).data
        this.baseDadosOffProvider.insertPessoas(this.listaPessoas)
      })
  }

  public async consultaListaSubGruposServicos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/subGrupoServicoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaSubGruposServicos = await JSON.parse(response._body).data
        this.baseDadosOffProvider.insertSubGruposServicos(this.listaSubGruposServicos)

      })
  }

  public async consultaListaServicos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/servicoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaServicos = await JSON.parse(response._body).data
        this.baseDadosOffProvider.insertServicos(this.listaServicos)
      })
  }

  public async consultaListaGrupoProdutos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/grupoProdutoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaGrupoProdutos = await JSON.parse(response._body).data
        this.baseDadosOffProvider.insertGrupoProdutos(this.listaGrupoProdutos)
      })
  }

  public async consultaListaSubGrupoProdutos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/subGrupoProdutoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaSubGrupoProdutos = await JSON.parse(response._body).data

        this.baseDadosOffProvider.insertSubGrupoProdutos(this.listaSubGrupoProdutos)
      })
  }

  public async consultaListaProdutos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/produtoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaProdutos = await JSON.parse(response._body).data
        this.baseDadosOffProvider.insertProdutos(this.listaProdutos)

        this.baseDadosOffProvider.getAllProdutos()
          .then((result : any) =>{
            this.produtos = result
          })
          .catch(()=>{
            this.toast.create({ message: 'Erro ao consultar Produtos.', duration: 3000, position: 'botton' }).present();
          })

      })
  }

  public async consultaServicosAgendas(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/agenda/servicoall`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaServicosAgendas = await JSON.parse(response._body)

        this.baseDadosOffProvider.insertServicosAgendas(this.listaServicosAgendas)

      })
  }

  private async consultaListaAgendas(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/agendaFindByPiscineiroLogin`,
      { headers: header })
      .subscribe(async (response: any) => {
        console.log(header);

        this.listaAgendasAux = await JSON.parse(response._body).data
        console.log(this.listaAgendasAux);

        this.baseDadosOffProvider.insertAgendas(this.listaAgendasAux)

        this.baseDadosOffProvider.getAllAgendas()
          .then((result: any[]) => {
            this.agendas = result;
          })
          .catch(() => {
            this.toast.create({ message: 'Erro ao carregar os agendas.', duration: 3000, position: 'botton' }).present();
          });

        this.montaListaAgenda(this.listaAgendasAux)

      })
  }

  private montaListaAgenda(lista: Array<any>): void {
    if (this.situacao == "Aberto") {
      for (let i = 0; i < lista.length; i++) {
        if (lista[i].status == "NAOATENDIDO") {
          this.listaAgendas.unshift({
            nomecliente: lista[i].nomecliente,
            datainicio: lista[i].dataagendamento ? moment(lista[i].dataagendamento).format('DD/MM/YYYY') : '(Não informado)',
            horainicio: lista[i].dataagendamento ? moment(lista[i].dataagendamento).format('HH:mm') : '(Não informado)',
            datatermino: lista[i].datatermino || null,
            idCliente: lista[i].idcliente,
            idPiscineiro: lista[i].idpiscineiro,
            idAgenda: lista[i].id,
            idPiscina: lista[i].idpiscina,
            situacao: "Aberto"
          })
        }
      }
    }
    if (this.situacao == "Encerrado") {
      for (let i = 0; i < lista.length; i++) {
        if (lista[i].status == "ATENDIDO") {
          this.listaAgendas.unshift({
            nomecliente: lista[i].nomecliente,
            datainicio: lista[i].dataagendamento ? moment(lista[i].dataagendamento).format('DD/MM/YYYY') : '(Não informado)',
            horainicio: lista[i].dataagendamento ? moment(lista[i].dataagendamento).format('HH:mm') : '(Não informado)',
            datatermino: lista[i].datatermino || null,
            idCliente: lista[i].idcliente,
            idPiscineiro: lista[i].idpiscineiro,
            idAgenda: lista[i].id,
            idPiscina: lista[i].idpiscina,
            situacao: "Encerrado"
          })
        }
      }
    }

    this.listaAgendasState = this.listaAgendas

    this.manipulaParaListaAgenda(this.exibeAberto)

    this.filtrarData(this.dataFiltro)
  }

  private manipulaParaListaAgenda(exibeAberto: boolean): void {
    exibeAberto ? this.agendaAberta() : this.agendaEncerrados()
  }

  private agendaAberta(): void {
    this.mensagemToogle = 'Consultar encerrados'
    this.situacao = "Aberto"
    this.listaAgendas = []

    for (let i = 0; i < this.listaAgendasAux.length; i++) {
      if (!this.listaAgendasAux[i].datatermino) {
        this.listaAgendas.unshift({
          nomecliente: this.listaAgendasAux[i].nomecliente,
          datainicio: this.listaAgendasAux[i].dataagendamento ? moment(this.listaAgendasAux[i].dataagendamento).format('DD/MM/YYYY') : '(Não informado)',
          horainicio: this.listaAgendasAux[i].dataagendamento ? moment(this.listaAgendasAux[i].dataagendamento).format('HH:mm') : '(Não informado)',
          datatermino: this.listaAgendasAux[i].datatermino || null,
          idCliente: this.listaAgendasAux[i].idcliente,
          idPiscineiro: this.listaAgendasAux[i].idpiscineiro,
          idAgenda: this.listaAgendasAux[i].id,
          idPiscina: this.listaAgendasAux[i].idpiscina,
          situacao: "Aberto"
        })
      }
    }

    if (this.status == "Dia") {
      this.listaAgendasState = this.listaAgendas
      this.listaAgendas = this.listaAgendas.filter(agenda => agenda.datainicio === this.dataFiltro)
    }
  }

  private agendaEncerrados(): void {
    this.mensagemToogle = 'Consultar abertos'
    this.situacao = "Encerrado"
    this.listaAgendas = []
    for (let i = 0; i < this.listaAgendasAux.length; i++) {
      if (this.listaAgendasAux[i].datatermino) {
        this.listaAgendas.unshift({
          nomecliente: this.listaAgendasAux[i].nomecliente,
          datainicio: this.listaAgendasAux[i].dataagendamento ? moment(this.listaAgendasAux[i].dataagendamento).format('DD/MM/YYYY') : '(Não informado)',
          horainicio: this.listaAgendasAux[i].dataagendamento ? moment(this.listaAgendasAux[i].dataagendamento).format('HH:mm') : '(Não informado)',
          datatermino: this.listaAgendasAux[i].datatermino || null,
          idCliente: this.listaAgendasAux[i].idcliente,
          idPiscineiro: this.listaAgendasAux[i].idpiscineiro,
          idAgenda: this.listaAgendasAux[i].id,
          idPiscina: this.listaAgendasAux[i].idpiscina,
          situacao: "Encerrado"
        })
      }
    }
    if (this.status == "Dia") {
      this.listaAgendasState = this.listaAgendas
      this.listaAgendas = this.listaAgendas.filter(agenda => agenda.datainicio === this.dataFiltro)
    }
  }

  goToOtherPage(objSelecionado) {
    if (objSelecionado.situacao == "Aberto") {
      var jsonAux = JSON.stringify(objSelecionado);
      localStorage.setItem('agendaSelecionada', jsonAux)
      this.navCtrl.push('IniciarTratamentoPage')
    }else if (objSelecionado.situacao == "Encerrado") {
      this.toastCtrl.create({
        message : 'Agenda já encerrada!',
        duration : 2000,
        position : 'top'
      }).present();
    }
  }

  protected filtrarData(evento: string): void {
    this.listaAgendas = this.listaAgendasState
    this.dataFiltro = evento
    this.dataFiltro = moment(this.dataFiltro).format('DD/MM/YYYY')
    this.listaAgendas = this.listaAgendas.filter(agenda => agenda.datainicio === this.dataFiltro)
  }

  protected reiniciaLista(): void {
    if (this.status == "Dia") {
      this.status = "Todos"
      this.mensagemToogleAgenda = "Consultar agendas do dia"
      this.listaAgendas = this.listaAgendasState
    }else{
      this.status = "Dia"
      this.mensagemToogleAgenda = "Consultar todas as agendas"
      this.listaAgendas = this.listaAgendas.filter(agenda => agenda.datainicio === this.dataFiltro)
    }
  }

  logOut() {
    localStorage.clear()
    this.navCtrl.setRoot('TelaLoginPage');
  }

  trocarPerfil() {
    this.navCtrl.setRoot('TelaPerfilPage');
  }

}

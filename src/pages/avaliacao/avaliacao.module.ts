import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvaliacaoPage } from './avaliacao';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    AvaliacaoPage,
  ],
  imports: [
    IonicPageModule.forChild(AvaliacaoPage),
    Ionic2RatingModule
  ],
})
export class AvaliacaoPageModule {}

import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { Http } from '@angular/http';
import { verify } from 'jsonwebtoken';
import header from './../../utils/headers';
import moment from 'moment'
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-avaliacao',
  templateUrl: 'avaliacao.html',
})
export class AvaliacaoPage {
  @ViewChild('mySlider') mySlider: any;

  public myForm: FormGroup;
  protected dadosCliente: any
  protected avaliacao: any
  public listaAgendasAtendidas = []
  public agendasAtendida = []
  private servicosAgenda = []
  private servicosAgendaAux = []
  private imagemAgenda = ''
  private idAgenda = ''
  public dataAtendimento : string
  public horaAtendimento : string

  observacao = ''

  rate1: any = 0;
  rate2: any = 0;
  rate3: any = 0;
  rate4: any = 0;

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public http: Http, private fb: FormBuilder) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(false, 'piscineiro');
  }

  protected ngOnInit(): void {
    this.myForm = this.fb.group({
      avaliacao: this.fb.array([Validators.compose([
        Validators.required
      ])]),
      observacao: ['', Validators.compose([
        Validators.required
      ])],
    });

    let jsonTarefa3 = window.localStorage.getItem('dadosCliente')
    this.dadosCliente = JSON.parse(jsonTarefa3)

    let tokenDecodificado: any = verify(localStorage.getItem('token'), 'keyClickCleanPoollllLoginXXX')
    this.consultaTodosDadosCliente(tokenDecodificado.pessoaId)
  }

  private async consultaTodosDadosCliente(pessoaId: string): Promise<any> {
    let dadosClienteJSON: any
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/fisicaFindById?entityId=${pessoaId}`,
      { headers: header })
      .subscribe(async (resposta: any) => {
        dadosClienteJSON = JSON.parse(resposta._body)
        await localStorage.setItem('dadosCliente', JSON.stringify(dadosClienteJSON.data[0]))
        this.consultaTodasAgendasCliente()
      })
  }

  private async consultaTodasAgendasCliente() {
    await this.http.get('http://cleanpoolbackend.clicksistemas.com.br/query/agendaFindByAtendidoLogin', { headers: header })
      .subscribe((resposta: any): any => {
        this.listaAgendasAtendidas = JSON.parse(resposta._body).data
        
        this.listaAgendasAtendidas = this.listaAgendasAtendidas.filter(agenda => !agenda.avaliada)
        console.log((this.listaAgendasAtendidas));
        
        this.http.get('http://cleanpoolbackend.clicksistemas.com.br/query/agendaFindById?entityId='+this.listaAgendasAtendidas[0].id, { headers: header })
          .subscribe((resposta: any): any => {
            this.agendasAtendida = JSON.parse(resposta._body).data
            console.log(this.agendasAtendida);

            this.dataAtendimento = moment(this.agendasAtendida[0].datainicio).format('DD/MM/YYYY')
            
            this.horaAtendimento = moment(this.agendasAtendida[0].datainicio).format('HH:mm')
            
            
          })

        this.listaAgendasAtendidas.length > 0
          ? this.consultaDadosAgenda(this.listaAgendasAtendidas[0].id)
          : this.navCtrl.setRoot('HomeClientePage')
      })
  }

  private consultaDadosAgenda(id: string) {
    this.idAgenda = id

    this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/agendaServicoFindByAgendaId?entityId=${id}`,
      { headers: header })
      .subscribe((resposta: any) => {
        this.servicosAgendaAux = JSON.parse(resposta._body).data
        this.servicosAgendaAux = this.servicosAgendaAux.filter(servico => servico.statusagendaservico == "ATENDIDO")
        for (let i = 0; i < this.servicosAgendaAux.length; i++) {
          this.servicosAgenda.push({
            id: this.servicosAgendaAux[i].id,
            nome: this.servicosAgendaAux[i].nome,
            nota: this.servicosAgendaAux[i].nome
          })
        }
        
      })


    this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/imagemFindByAgendaId?entityId=${id}`,
      { headers: header })
      .subscribe((resposta: any) => {
        this.imagemAgenda = JSON.parse(resposta._body).data
      })
  }

  onModelChange(event, id) {
    const avaliacaoFormArray = <FormArray>this.myForm.controls.avaliacao;

    if (this.myForm.controls.avaliacao.value.length === 0) {
      avaliacaoFormArray.push(new FormControl({
        servico: id,
        nota: event
      }));
    } else {
      const index = avaliacaoFormArray.value.findIndex(avaliacao => avaliacao.id === id)
      if (index === -1) {
        avaliacaoFormArray.push(new FormControl({
          servico: id,
          nota: event
        }));
      } else {
        avaliacaoFormArray.removeAt(index);
        avaliacaoFormArray.push(new FormControl({
          servico: id,
          nota: event
        }));
      }
    }

  }

  async goToOtherPage() {
    const objAgenda = {
      agenda: {
        id: this.idAgenda
      },
      observacao: this.observacao,
      avaliacaoservicos: this.myForm.value.avaliacao
    }

    await this.http.post('http://cleanpoolbackend.clicksistemas.com.br/avaliacao', JSON.stringify(objAgenda), { headers: header })
      .subscribe((resposta: any): any => {
        if (this.listaAgendasAtendidas.length > 1) {
          this.navCtrl.push('AvaliacaoPage')
        } else {
          this.navCtrl.setRoot('HomeClientePage');
        }
      })


    // this.myForm.value.avaliacao
    // this.navCtrl.setRoot('HomeClientePage');
  }

  slideNext() {
    this.mySlider.slideNext();
  }

  slidePrev() {
    this.mySlider.slidePrev();
  }

}

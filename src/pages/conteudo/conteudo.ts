import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http'
import header from './../../utils/headers'

@IonicPage()
@Component({
  selector: 'page-conteudo',
  templateUrl: 'conteudo.html',
})
export class ConteudoPage {

  private listaConteudo: Array<any> = []

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  async ngOnInit(): Promise<any> {
    this.consultaListaConteudos()
  }
  

  private async consultaListaConteudos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/noticiaFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaConteudo = await JSON.parse(response._body).data
      })
  }

  logOut(){
    localStorage.clear()   

    this.navCtrl.setRoot('TelaLoginPage');
  }
}

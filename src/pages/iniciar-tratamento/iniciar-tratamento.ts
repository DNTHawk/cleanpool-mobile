import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, PopoverController, ToastController } from 'ionic-angular';
import { Http } from '@angular/http'
import header from './../../utils/headers'
import { Network } from '@ionic-native/network';
import { BaseDadosOffProvider } from '../../providers/base-dados-off/base-dados-off';

@IonicPage()
@Component({
  selector: 'page-iniciar-tratamento',
  templateUrl: 'iniciar-tratamento.html',
})

export class IniciarTratamentoPage {

  private API_URL = 'http://cleanpoolbackend.clicksistemas.com.br'
  protected clientePF: any
  protected clientePJ: any
  protected cliente: any
  protected piscineiro: Array<any>
  protected contatoCliente: Array<any>
  protected piscinaCliente: Array<any>
  private agenda: any
  private idCliente: string
  private idPiscineiro: string
  private idPiscina: string
  private idAgenda: string
  private endereco: string
  private numeroContato: string
  private litragem: number

  public conexao: String = "Online"

  public piscinas: Array<any> = [];
  public pessoas: Array<any> = [];

  protected dadosUsuario: any

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public menuCtrl: MenuController, public popoverCtrl: PopoverController, public http: Http,
    public network: Network, private toast: ToastController,
    public baseDadosOffProvider: BaseDadosOffProvider,) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(true, 'piscineiro');
  }

  async ngOnInit(): Promise<any> {

    this.network.onDisconnect().subscribe(() => {
      this.conexao = "Offline"
    }, error => console.log(error));

    this.network.onConnect().subscribe(() => {
      this.conexao = "Online"
    }, error => console.log(error));

    if (this.conexao == "Online") {
      this.modoOnline()
    }else if (this.conexao == "Offline"){
      this.modoOffline()
    }
  }

  modoOnline(){

    let jsonTarefa1 = window.localStorage.getItem('dadosUsuario')
    this.dadosUsuario = JSON.parse(jsonTarefa1)

    let jsonTarefa = window.localStorage.getItem('agendaSelecionada')
    this.agenda = JSON.parse(jsonTarefa)

    this.idCliente = this.agenda.idCliente
    this.idPiscineiro = this.agenda.idPiscineiro
    this.idAgenda = this.agenda.idAgenda
    this.idPiscina = this.agenda.idPiscina

    this.consultaClientePF()
    this.consultaClientePJ()
    this.consultaContatoCliente()
    this.consultaPiscinaCliente()

  }

  modoOffline(){
    this.baseDadosOffProvider.getAllPiscinas()
      .then((result: Array<any>) => {
        this.piscinas = result;

        let jsonTarefa = window.localStorage.getItem('agendaSelecionada')
        this.agenda = JSON.parse(jsonTarefa)

        this.piscinas = this.piscinas.filter(piscina => piscina.id == this.agenda.idPiscina)

        this.piscinaCliente = this.piscinas

        this.litragem = this.piscinaCliente[0].volumeagua.toFixed(0)

        var jsonAux = JSON.stringify(this.piscinaCliente);
        localStorage.setItem('dadosPiscina', jsonAux)
      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao carregar as Piscinas.', duration: 3000, position: 'botton' }).present();
      });

    this.baseDadosOffProvider.getAllPessoas()
      .then((result: Array<any>) => {
        this.pessoas = result;

        let jsonTarefa1 = window.localStorage.getItem('dadosUsuario')
        this.dadosUsuario = JSON.parse(jsonTarefa1)

        let jsonTarefa = window.localStorage.getItem('agendaSelecionada')
        this.agenda = JSON.parse(jsonTarefa)

        this.pessoas = this.pessoas.filter(pessoa => pessoa.idUser == this.agenda.idCliente)

        if (this.pessoas[0].tipo == "FISICA") {
          this.clientePF = this.pessoas
        }else{
          this.clientePJ = this.pessoas
        }

      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao carregar as pessoas.', duration: 3000, position: 'botton' }).present();
      });


  }

  private async consultaClientePF(): Promise<any> {
    await this.http.get(`${this.API_URL}/query/fisicaFindById?entityId=${this.idCliente}`,
    { headers: header })
      .subscribe(async (response: any) => {
        this.clientePF = await JSON.parse(response._body).data
      })
  }

  private async consultaClientePJ(): Promise<any> {
    await this.http.get(`${this.API_URL}/query/juridicaFindById?entityId=${this.idCliente}`,
    { headers: header })
      .subscribe(async (response: any) => {
        this.clientePJ = await JSON.parse(response._body).data
      })
  }

  private async consultaContatoCliente(): Promise<any> {
    await this.http.get(`${this.API_URL}/query/pessoaTelefoneFindByPessoaId?entityId=${this.idCliente}`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.contatoCliente = await JSON.parse(response._body).data

        this.numeroContato = this.contatoCliente[0].numero
        this.numeroContato = this.numeroContato.replace('(', '')
        this.numeroContato = this.numeroContato.replace(')', '')
        this.numeroContato = this.numeroContato.replace(/( )/g, '')
        this.numeroContato = this.numeroContato.replace('-', '')
      })
  }

  private async consultaPiscinaCliente(): Promise<any> {
    await this.http.get(this.API_URL + '/query/piscinaFindById?entityId=' + this.idPiscina,
      { headers: header })
      .subscribe(async (response: any) => {
        this.piscinaCliente = await JSON.parse(response._body).data

        var jsonAux = JSON.stringify(this.piscinaCliente);
        localStorage.setItem('dadosPiscina', jsonAux)

        this.litragem = this.piscinaCliente[0].volumeagua.toFixed(0)
        this.endereco = this.piscinaCliente[0].logradouro+" "+this.piscinaCliente[0].numero+", "+this.piscinaCliente[0].bairro+", "+this.piscinaCliente[0].cidade+" "+this.piscinaCliente[0].siglaunidadefederativa
        this.endereco = this.endereco.replace(/( )/g, '+')
      }, async (error: any) => {
        if (error.status === 0) {
          this.modoOffline()
        }
      })
  }

  goToOtherPage() {
    this.navCtrl.push('ServicoPage');
  }

  abrirMapa() {
    this.navCtrl.push('MapaRotaPage');
  }

}

import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Http } from '@angular/http'
import header from './../../utils/headers'
import { Geolocation } from '@ionic-native/geolocation'

declare var google;

@IonicPage()
@Component({
  selector: 'page-mapa-rota',
  templateUrl: 'mapa-rota.html',
})
export class MapaRotaPage {

  protected dadosPiscina: Array<any>
  private agenda: any
  private idPiscina: string

  directionsService = new google.maps.DirectionsService()
  directionsDisplay = new google.maps.DirectionsRenderer()
  map: any;
  // startPosition: any;
  originPosition: string;
  destinationPosition: string;

  constructor(public http: Http, private geolocation: Geolocation) {
  }

  async ngOnInit(): Promise<any> {

    let jsonTarefa = window.localStorage.getItem('agendaSelecionada')

    this.agenda = JSON.parse(jsonTarefa)
    
    this.idPiscina = this.agenda.idPiscina

    this.consultaPiscina()
    
  }

  private async consultaPiscina(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/piscinaFindById?entityId=`+this.idPiscina,
      { headers: header })
      .subscribe(async (response: any) => {
        this.dadosPiscina = await JSON.parse(response._body).data
    })

  }

  

  

  ionViewDidLoad() {
    this.initializeMap();
    
  }

  initializeMap() {

    this.geolocation.getCurrentPosition().then((resp) => {
      const position = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude)

      const mapOptions = {
        zoom: 18,
        center: this.originPosition,
      }

      this.map = new google.maps.Map(document.getElementById('map'), mapOptions)

      // const marker = new google.maps.Marker({
      //   position: position,
      //   map: this.map
      // })

      this.directionsDisplay.setMap(this.map)

      // this.startPosition = new google.maps.LatLng(-23.422711, -51.941990);
      this.originPosition = position
      this.destinationPosition = new google.maps.LatLng(-23.421922, -51.940041)

      this.calculateRoute();

     }).catch((error) => {
       console.log('Error getting location', error);
     });

    // const marker = new google.maps.Marker({
    //   position: this.originPosition,
    //   map: this.map,
    // })

  }

  calculateRoute() {
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      this.originPosition = new google.maps.LatLng(data.coords.latitude, data.coords.longitude)
    });


    if (this.destinationPosition && this.originPosition) {
      const request = {
        origin: this.originPosition,
        destination: this.destinationPosition,
        travelMode: 'DRIVING'
      };

      this.traceRoute(this.directionsService, this.directionsDisplay, request)
    }
  }

  traceRoute(service: any, display: any, request: any) {
    service.route(request, function (result, status) {
      if (status == 'OK') {
        display.setDirections(result)
      }
    });
  }

}

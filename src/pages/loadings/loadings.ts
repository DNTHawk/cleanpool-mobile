import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, MenuController } from 'ionic-angular';
import { LoginProvider } from '../../providers/login/login';
import { BaseDadosOffProvider } from '../../providers/base-dados-off/base-dados-off';
import { Network } from '@ionic-native/network';
import { Http, Response } from '@angular/http'
import header from './../../utils/headers'
import { PreLoadingPage } from '../pre-loading/pre-loading';

@IonicPage()
@Component({
  selector: 'page-loadings',
  templateUrl: 'loadings.html',
})
export class LoadingsPage {

  public conexao: String = "Online"
  public agendaFinalizada: Array<any> = []
  public listaProdutosSugeridos: Array<any> = []
  public listaServicosAtendidos: Array<any> = []
  private agendasFinalizadas: any
  private produtosAgendas: Array<any> = [];
  public servicosAgendas: Array<any>=[]
  public servicosAgendasAux: Array<any>=[]
  private produtosAgendasAux: Array<any> = [];
  public listaPiscinas: Array<any> = [];

  constructor(public navCtrl: NavController,private toastCtrl: ToastController ,
    public navParams: NavParams, public menuCtrl: MenuController, public http: Http,
    public loginProvider: LoginProvider, private toast: ToastController,
    public baseDadosOffProvider: BaseDadosOffProvider, public network: Network) {
  }

  async ngOnInit(): Promise<any> {
    this.network.onDisconnect().subscribe(() => {
      this.conexao = "Offline"
    }, error => console.log(error));

    this.network.onConnect().subscribe(() => {
      this.conexao = "Online"
    }, error => console.log(error));

    if (this.conexao == "Online") {
      this.modoOnline()
    }else if (this.conexao == "Offline"){
      this.modoOffline()
    }

  }

  modoOnline(){
    this.consultaListaPiscinas()
  }

  public async consultaListaPiscinas(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/piscina/all`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaPiscinas = await JSON.parse(response._body)

        this.verificaAgendaPendente()

      }, async (error: any) => {
        if (error.status === 0) {
          this.modoOffline()
        }
      })
  }

  modoOffline(){
    this.navCtrl.setRoot('AgendaPage')
  }

  public async verificaAgendaPendente(){
    this.baseDadosOffProvider.getAllFinalizarAgenda()
      .then((result : any) => {
        this.agendaFinalizada = result

        if (this.agendaFinalizada.length > 0) {
          for (let i = 0; i < this.agendaFinalizada.length; i++) {
            this.baseDadosOffProvider.getProdutosSugeridos(this.agendaFinalizada[i].id)
              .then((result : any) => {
                this.listaProdutosSugeridos = result
                this.listaProdutosSugeridos = this.listaProdutosSugeridos.filter(produto => produto.idAgenda == this.agendaFinalizada[i].id)

                var produtosAgendas = []
                produtosAgendas.length = 0
                for (let j = 0; j < this.listaProdutosSugeridos.length; j++) {
                  produtosAgendas[j] = {
                    quantidade: parseInt(this.listaProdutosSugeridos[j].quantidade),
                    produto: {
                      id: this.listaProdutosSugeridos[j].id,
                    }
                  }
                }

                this.baseDadosOffProvider.getServicosAtendidos(this.agendaFinalizada[i].id)
                  .then((result : any) => {
                    this.listaServicosAtendidos = result
                    this.listaServicosAtendidos = this.listaServicosAtendidos.filter(servico => servico.idAgenda == this.agendaFinalizada[i].id)

                    var servicosAgendas = []
                    servicosAgendas.length = 0
                    for (let k = 0; k < this.listaServicosAtendidos.length; k++) {
                      servicosAgendas[k]= {
                        servico: {
                          id: this.listaServicosAtendidos[k].id,
                        },
                        status: this.listaServicosAtendidos[k].status
                      }
                    }

                    this.agendasFinalizadas = {
                      id: this.agendaFinalizada[i].id,
                      observacao: this.agendaFinalizada[i].observacao,
                      datainicio: this.agendaFinalizada[i].datainicio,
                      datatermino: this.agendaFinalizada[i].datatermino,
                      items: produtosAgendas,
                      servicos: servicosAgendas,
                      status: "ATENDIDO"
                    }

                    setTimeout(() => {
                      this.realizarUpdate()
                    }, 1000);
                  })
              })
          }
        } else {
          this.navCtrl.setRoot('AgendaPage')
        }
      })
      .catch(()=>{
        this.navCtrl.setRoot('AgendaPage')
      })
  }

  private async realizarUpdate(): Promise<any> {
    return await this.http.put('http://cleanpoolbackend.clicksistemas.com.br/agenda/finalizar/',
      JSON.stringify(this.agendasFinalizadas), { headers: header })
      .subscribe(
        (response: Response) => {
          this.baseDadosOffProvider.deleteAgenda(this.agendasFinalizadas)
            .then(()=>{
              this.toast.create({ message: 'Update com Suscesso', duration: 3000, position: 'botton' }).present();
              this.navCtrl.setRoot('PreLoadingPage')
            })
        }, async (error: any) => {
          if (error.status === 0) {
            this.modoOffline()
          }
      })
  }
  mudaPagina(){
    this.navCtrl.setRoot('AgendaPage')
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SugerirProdutosPage } from './sugerir-produtos';

@NgModule({
  declarations: [
    SugerirProdutosPage,
  ],
  imports: [
    IonicPageModule.forChild(SugerirProdutosPage),
  ],
})
export class SugerirProdutosPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular'
import { Http } from '@angular/http'
import header from './../../utils/headers'
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { BaseDadosOffProvider } from '../../providers/base-dados-off/base-dados-off';
import { Network } from '@ionic-native/network';

@IonicPage()
@Component({
  selector: 'page-sugerir-produtos',
  templateUrl: 'sugerir-produtos.html',
})
export class SugerirProdutosPage {

  public myForm: FormGroup;

  private listaGruposProdutos: Array<any>
  private listaSubGruposProdutos: Array<any>
  private listaProdutos: Array<any>
  private listaProdutosState: Array<any> = []
  private dataFiltro: string

  public conexao: String = "Online"

  public subGrupoProdutos: Array<any> = [];
  public grupoProdutos: Array<any> = [];
  public produtos: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http,
    public menuCtrl: MenuController, private fb: FormBuilder, private toast: ToastController,
    public baseDadosOffProvider: BaseDadosOffProvider, public network: Network) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(false, 'piscineiro');
  }


  async ngOnInit(): Promise<any> {

    this.network.onDisconnect().subscribe(() => {
      this.conexao = "Offline"
    }, error => console.log(error));

    this.network.onConnect().subscribe(() => {
      this.conexao = "Online"
    }, error => console.log(error));

    if (this.conexao == "Online") {
      this.modoOnline()
    }

    this.myForm = this.fb.group({
      produto: this.fb.array([]),
    });

  }

  modoOnline(){
    this.consultaListaProdutos()
    this.consultaGruposProdutos()
    this.consultaSubGruposProdutos()
  }

  modoOffline(){
    this.baseDadosOffProvider.getAllSubGrupoProdutos()
      .then((result: Array<any>) => {
        this.subGrupoProdutos = result;

        this.listaSubGruposProdutos = this.subGrupoProdutos

      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao carregar as subGrupoProdutos.', duration: 3000, position: 'botton' }).present();
      });

    this.baseDadosOffProvider.getAllGrupoProdutos()
      .then((result: Array<any>) => {
        this.grupoProdutos = result;

        this.listaGruposProdutos = this.grupoProdutos

      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao carregar as subGrupoProdutos.', duration: 3000, position: 'botton' }).present();
      });

    this.baseDadosOffProvider.getAllProdutos()
      .then((result: Array<any>) => {
        this.produtos = result;

        this.listaProdutos = this.produtos
        this.listaProdutosState = this.listaProdutos

      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao carregar as subGrupoProdutos.', duration: 3000, position: 'botton' }).present();
      });
  }

  onChange(produto: string, value: number) {
    const produtoFormArray = <FormArray>this.myForm.controls.produto;

    if (value) {
      produtoFormArray.push(new FormControl({
        quantidade: value,
        produto:{
          id: produto,
        }
      }));
    } else {
      let index = produtoFormArray.controls.findIndex(x => x.value == produto)
      produtoFormArray.removeAt(index);
    }
  }

  private async consultaListaProdutos(): Promise<any> {

    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/produtoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaProdutos = await JSON.parse(response._body).data
        this.listaProdutosState = this.listaProdutos
        console.log(this.listaProdutos);
      }, async (error: any) => {
        if (error.status === 0) {
          this.modoOffline()
        }
      })
  }

  private async consultaGruposProdutos(): Promise<any> {

    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/grupoProdutoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaGruposProdutos = await JSON.parse(response._body).data
      })
  }

  private async consultaSubGruposProdutos(): Promise<any> {

    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/subGrupoProdutoFindByParameter?parameter=&number=1`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaSubGruposProdutos = await JSON.parse(response._body).data
      })
  }

  goToPageHome() {

    let produtosSugeridos = JSON.stringify(this.myForm.value.produto);

    localStorage.setItem('produtosSugeridos', produtosSugeridos)

    this.navCtrl.push('Servico2Page');
  }

  filtraItens(evento: any) {
    this.listaProdutos = this.listaProdutosState
    const val = evento.target.value

    if (val && val.trim() !== '') {
      this.listaProdutos = this.listaProdutos.filter(produto => (produto.nome.toLowerCase().indexOf(val.toLowerCase()) > -1))
    }
  }

}

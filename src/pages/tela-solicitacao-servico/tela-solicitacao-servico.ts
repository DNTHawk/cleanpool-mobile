import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Http, Response } from '@angular/http'
import header from './../../utils/headers'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-tela-solicitacao-servico',
  templateUrl: 'tela-solicitacao-servico.html',
})
export class TelaSolicitacaoServicoPage {

  protected listaServicos: Array<any>
  protected listaSubGruposServicos: Array<any>
  private listaServicosState: Array<any> = []
  // private qtdProdutos: string
  public myForm: FormGroup;
  servicoChecked: any[];
  selecionado: any;
  private listaIdServicos = []
  protected dadosCliente: any = []
  protected solicitacao: any

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public navParams: NavParams, public http: Http, private fb: FormBuilder) { 
    this.listaIdServicos = [];
  }

  async ngOnInit(): Promise<any> {

    let jsonTarefa = window.localStorage.getItem('dadosCliente')
    this.dadosCliente = JSON.parse(jsonTarefa)

    this.myForm = this.fb.group({
      servico: this.fb.array([]),
    });

    this.consultaListaServicos()
    this.consultaSubGruposServicos()
  }

  onChange(servico: string, isChecked: boolean) {
    if (isChecked) {
      this.listaIdServicos.push(servico)
    } else {
      let index = this.listaIdServicos.findIndex(x => x.value == servico)
      this.listaIdServicos.splice(index);
    }
  }

  private async consultaListaServicos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/servicoFindByParameter?parameter=&number=1&size=10`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaServicos = await JSON.parse(response._body).data

        this.listaServicosState = this.listaServicos
      })
  }

  private async consultaSubGruposServicos(): Promise<any> {
    await this.http.get(`http://cleanpoolbackend.clicksistemas.com.br/query/subGrupoServicoFindByParameter?parameter=&number=1&size=10`,
      { headers: header })
      .subscribe(async (response: any) => {
        this.listaSubGruposServicos = await JSON.parse(response._body).data
      })
  }

  filtraItens(evento: any) {
    this.listaServicos = this.listaServicosState
    const val = evento.target.value

    if (val && val.trim() !== '') {
      this.listaServicos = this.listaServicos.filter(produto => (produto.nome.toLowerCase().indexOf(val.toLowerCase()) > -1))
    }
  }

  goToPageHome() {
    this.navCtrl.setRoot('HomeClientePage');
  }
  

  goToOtherPageSolicitar(){

    let servicos = []
    let solicitacao = {}

    this.listaIdServicos.forEach(servico => {
      servicos.push({
        id: servico,
      })
    })

    solicitacao = {
      cliente:{
        id: this.dadosCliente.id,
      },
      servicos: servicos,
    }

    this.solicitacao = solicitacao

    this.realizarUpdate()
  }

  private async realizarUpdate(): Promise<any> {
    return await this.http.post('http://cleanpoolbackend.clicksistemas.com.br/sugestaoservico/',
      JSON.stringify(this.solicitacao), { headers: header })
      .subscribe(
        (response: Response) => {
          console.log(response)
          let alert = this.alertCtrl.create(
            {
              message: 'Logo entraremos em contato para confirmar a sua solicitação',
              buttons: ['OK']
            }
          )
          alert.present()
          this.navCtrl.setRoot('HomeClientePage');
        },
        (error: Error) => {
          console.log(error)
        }
      )
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TelaSolicitacaoServicoPage } from './tela-solicitacao-servico';

@NgModule({
  declarations: [
    TelaSolicitacaoServicoPage,
  ],
  imports: [
    IonicPageModule.forChild(TelaSolicitacaoServicoPage),
  ],
})
export class TelaSolicitacaoServicoPageModule {}

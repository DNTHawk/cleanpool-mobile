import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Http, Response } from '@angular/http';
import header from './../../utils/headers'

@IonicPage()
@Component({
  selector: 'page-add-foto',
  templateUrl: 'add-foto.html',
})
export class AddFotoPage {
  @ViewChild('mySlider') mySlider: any;

  private foto : any
  private quantidade : number
  private listaFotos : Array<any> = []
  protected imagensAtendimento: any
  protected dadosAgenda: any
  private idAgenda: string

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, private camera: Camera, private toastCtrl: ToastController, public http: Http) {
    this.menuCtrl.enable(false, 'cliente');
    this.menuCtrl.enable(true, 'piscineiro');
  }

  async ngOnInit(): Promise<any> {
    let jsonTarefa2 = window.localStorage.getItem('agendaSelecionada')
    this.dadosAgenda = JSON.parse(jsonTarefa2)
    this.idAgenda = this.dadosAgenda.idAgenda
  }
  
  tirarNovaFoto(){
    const options: CameraOptions = {
      quality : 100,
      destinationType : this.camera.DestinationType.DATA_URL,
      encodingType : this.camera.EncodingType.JPEG,
      mediaType : this.camera.MediaType.PICTURE, 
    }

    this.camera.getPicture(options).then((imageData)=>{
      let base64Image = 'data:image/jpeg;base64,' + imageData
      this.foto = base64Image;

      this.listaFotos.push({
        url: this.foto
      })

    }),(error)=>{
      this.toastCtrl.create({
        message : 'Não foi possivel tirar a foto!',
        duration : 2000,
        position : 'top'
      }).present();
    }
  }

  slideNext() {
    this.mySlider.slideNext();
  }

  slidePrev() {
    this.mySlider.slidePrev();
  }

  private async realizarUpdate(): Promise<any> {
    return this.http.put('http://cleanpoolbackend.clicksistemas.com.br/agenda/imagens/',
      JSON.stringify(this.imagensAtendimento), { headers: header })
      .subscribe(
        (response: Response) => {
          console.log(response)
          this.navCtrl.push('FinalizarAtendimentoPage');
        },
        (error: Error) => {
          console.log(error)
        }
      )
  }

  goToOtherPageFinalizar() {
    this.quantidade = this.listaFotos.length

    let quantidadeFotos = JSON.stringify(this.quantidade);

    localStorage.setItem('quantidadeFotos', quantidadeFotos)

    this.imagensAtendimento = {
      id: this.idAgenda,
      imagens: this.listaFotos
    }
    
    this.realizarUpdate()
  }
}

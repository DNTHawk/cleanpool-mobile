import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddFotoPage } from './add-foto';

@NgModule({
  declarations: [
    AddFotoPage,
  ],
  imports: [
    IonicPageModule.forChild(AddFotoPage),
  ],
})
export class AddFotoPageModule {}

import { Headers } from '@angular/http'

let headers = new Headers()

headers.append('Content-Type', 'application/json')
headers.append('Accept', 'application/json')
headers.append('Authorization', `Bearer ${localStorage.token}`)

export default headers
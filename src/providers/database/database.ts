import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { ToastController } from 'ionic-angular';
 
@Injectable()
export class DatabaseProvider {
 
  constructor(private sqlite: SQLite, private toast: ToastController) { }
 
  /**
   * Cria um banco caso não exista ou pega um banco existente com o nome no parametro
   */
  public getDB() {
    return this.sqlite.create({
      name: 'cleanpool.db',
      location: 'default'
    });
  }
 
  /**
   * Cria a estrutura inicial do banco de dados
   */
  public createDatabase(dados) {
    return this.getDB()
      .then((db: SQLiteObject) => {
 
        // Criando as tabelas
        this.createTables(db);
 
        // Inserindo dados padrão
        this.insert(db, dados);
 
      })
      .catch(e => console.log(e));
  }
 
  /**
   * Criando as tabelas no banco de dados
   * @param db
   */
  private createTables(db: SQLiteObject) {

    let sql = 'DROP TABLE IF EXISTS login';
    db.executeSql(sql)
    let sql2 = 'DROP TABLE IF EXISTS subGrupoServicos';
    db.executeSql(sql2)
    let sql3 = 'DROP TABLE IF EXISTS servicos';
    db.executeSql(sql3)
    let sql4 = 'DROP TABLE IF EXISTS grupoProdutos';
    db.executeSql(sql4)
    let sql5 = 'DROP TABLE IF EXISTS subGrupoProdutos';
    db.executeSql(sql5)
    let sql6 = 'DROP TABLE IF EXISTS produtos';
    db.executeSql(sql6)
    let sql7 = 'DROP TABLE IF EXISTS agendas';
    db.executeSql(sql7)
    let sql8 = 'DROP TABLE IF EXISTS piscinas';
    db.executeSql(sql8)
    let sql9 = 'DROP TABLE IF EXISTS servicosAgendas';
    db.executeSql(sql9)
    
    // let sql10 = 'DROP TABLE IF EXISTS finalizarAgenda';
    // db.executeSql(sql10)
    // let sql11 = 'DROP TABLE IF EXISTS produtosSugeridos';
    // db.executeSql(sql11)
    // let sql12 = 'DROP TABLE IF EXISTS servicosAtendidos';
    // db.executeSql(sql12)

    // Criando as tabelas
    db.sqlBatch([
      ['CREATE TABLE IF NOT EXISTS login (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, login TEXT, password TEXT, idUser TEXT, pessoa TEXT, tipo1 TEXT, tipo2 TEXT)'],
      ['CREATE TABLE IF NOT EXISTS pessoas (id integer primary key AUTOINCREMENT NOT NULL, tipo TEXT, nome TEXT, cnpj TEXT, datanascimento TEXT, inscricaoestadual TEXT, razaosocial TEXT, genero TEXT, cpf TEXT, idUser TEXT, email TEXT, inscricaomunicipal TEXT, status TEXT)'],
      ['CREATE TABLE IF NOT EXISTS subGrupoServicos (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, idSubGrupoServico TEXT, status TEXT)'],
      ['CREATE TABLE IF NOT EXISTS servicos (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, subGrupo TEXT, idServico TEXT, status TEXT)'],
      ['CREATE TABLE IF NOT EXISTS grupoProdutos (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, idGrupoProdutos TEXT, status TEXT)'],
      ['CREATE TABLE IF NOT EXISTS subGrupoProdutos (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, idSubGrupoProdutos TEXT, grupo TEXT, label1 TEXT, status TEXT)'],
      ['CREATE TABLE IF NOT EXISTS produtos (id TEXT, nome TEXT, grupo TEXT, subgrupo TEXT, unidade TEXT, url TEXT, status TEXT)'],
      ['CREATE TABLE IF NOT EXISTS agendas (datainicio TEXT, observacao TEXT, nomecliente TEXT, descricaopiscina TEXT, idpiscina TEXT, idpiscineiro TEXT, idcliente TEXT, nomepiscineiro TEXT, avaliada TEXT, formatopiscina TEXT, datatermino TEXT, id TEXT, dataagendamento TEXT, status TEXT)'],
      ['CREATE TABLE IF NOT EXISTS piscinas (id TEXT, descricao TEXT, formato TEXT, logradouro TEXT, numero TEXT, bairro TEXT, cep TEXT, cidade TEXT, siglaunidadefederativa TEXT, pais TEXT, tipoendereco TEXT, clienteId TEXT, volumeagua INTEGER, status TEXT)'],
      ['CREATE TABLE IF NOT EXISTS servicosAgendas (idAgenda TEXT, id TEXT, nome TEXT, subgrupo TEXT, statusagendaservico TEXT)'],
      ['CREATE TABLE IF NOT EXISTS fotos (id integer primary key AUTOINCREMENT NOT NULL, base64 TEXT, idAgenda TEXT)'],
      ['CREATE TABLE IF NOT EXISTS finalizarAgenda (id TEXT, observacao TEXT, datainicio TEXT, datatermino TEXT, status TEXT)'],
      ['CREATE TABLE IF NOT EXISTS produtosSugeridos (id TEXT, idAgenda TEXT, quantidade TEXT)'],
      ['CREATE TABLE IF NOT EXISTS servicosAtendidos (id TEXT, idAgenda TEXT, status TEXT)']
    ])
      .then(() => {
        // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
      });   
  }
 
  /**
   * Incluindo os dados padrões
   * @param db
   */
  public insert(db: SQLiteObject, dados) {
    
    let sql = 'insert into login (nome, login, password, idUser, pessoa, tipo1, tipo2) values (?, ?, ?, ?, ?, ?, ?)';
    let data = [dados.nome, dados.login, dados.senha, dados.idUser, dados.tipo, dados.tipos[0], dados.tipos[1]];
 
    return db.executeSql(sql, data)
      .catch(() => {
        // this.toast.create({ message: 'Erro ao incluir os dados.', duration: 3000, position: 'botton' }).present();
      });
  }
}
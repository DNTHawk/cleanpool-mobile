import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject } from '@ionic-native/sqlite';
import header from './../../utils/headers';
import { Http, Response} from '@angular/http';
import { ToastController } from 'ionic-angular';


@Injectable()
export class BaseDadosOffProvider {

  constructor(private dbProvider: DatabaseProvider, public http: Http, private toast: ToastController) {
  }

  // -----------------------------\/Pessoas\/----------------------------------------------
  public insertPessoas(listaPessoas){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE pessoas';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS pessoas (id integer primary key AUTOINCREMENT NOT NULL, tipo TEXT, nome TEXT, cnpj TEXT, datanascimento TEXT, inscricaoestadual TEXT, razaosocial TEXT, genero TEXT, cpf TEXT, idUser TEXT, email TEXT, inscricaomunicipal TEXT, status TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaPessoas.length; i++) {
          let sql = 'insert into pessoas (tipo, nome, cnpj, datanascimento, inscricaoestadual, razaosocial, genero, cpf, idUser, email, inscricaomunicipal, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
          let data = [listaPessoas[i].tipo,listaPessoas[i].nome, listaPessoas[i].cnpj,
          listaPessoas[i].datanascimento,listaPessoas[i].inscricaoestadual,listaPessoas[i].razaosocial,
          listaPessoas[i].genero, listaPessoas[i].cpf, listaPessoas[i].id, listaPessoas[i].email, 
          listaPessoas[i].inscricaomunicipal, listaPessoas[i].status];

          db.executeSql(sql, data)
            .catch(() => {
              // this.toast.create({ message: 'Erro ao incluir os dados Pessoas.', duration: 3000, position: 'botton' }).present();
            });
        }
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }
  
  public getAllPessoas() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from pessoas', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let pessoas: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var pessoa = data.rows.item(i);
              pessoas.push(pessoa);
            }
            return pessoas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/SubGrupoServicos\/----------------------------------------------
  public insertSubGruposServicos(listaSubGruposServicos){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE subGrupoServicos';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS subGrupoServicos (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, idSubGrupoServico TEXT, status TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaSubGruposServicos.length; i++) {
          let sql = 'insert into subGrupoServicos (nome, idSubGrupoServico, status) values (?, ?, ?)';
          let data = [listaSubGruposServicos[i].nome,listaSubGruposServicos[i].id,
          listaSubGruposServicos[i].status];
      
          db.executeSql(sql, data)
            .catch(() => {
              // this.toast.create({ message: 'Erro ao incluir os dados SubGrupoServicos.', duration: 3000, position: 'botton' }).present();
            });
        }
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllSubGruposServicos() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from subGrupoServicos', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let subGruposServicos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var subGrupoServico = data.rows.item(i);
              subGruposServicos.push(subGrupoServico);
            }
            return subGruposServicos;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/Servicos\/----------------------------------------------
  public insertServicos(listaServicos){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE servicos';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS servicos (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, subGrupo TEXT, idServico TEXT, status TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaServicos.length; i++) {
          let sql = 'insert into servicos (nome, subGrupo, idServico, status) values (?, ?, ?, ?)';
          let data = [listaServicos[i].nome,listaServicos[i].subgrupo,
          listaServicos[i].id, listaServicos[i].status];
      
          db.executeSql(sql, data)
            .catch(() => {
              // this.toast.create({ message: 'Erro ao incluir os dados Servicos.', duration: 3000, position: 'botton' }).present();
            });
        }
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllServicos() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from servicos', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let servicos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var servico = data.rows.item(i);
              servicos.push(servico);
            }
            return servicos;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/GrupoProdutos\/----------------------------------------------
  public insertGrupoProdutos(listaGrupoProdutos){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE grupoProdutos';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS grupoProdutos (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, idGrupoProdutos TEXT, status TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaGrupoProdutos.length; i++) {
          let sql = 'insert into grupoProdutos (nome, idGrupoProdutos, status) values (?, ?, ?)';
          let data = [listaGrupoProdutos[i].nome, listaGrupoProdutos[i].id, listaGrupoProdutos[i].status];
      
          db.executeSql(sql, data)
            .catch(() => {
              // this.toast.create({ message: 'Erro ao incluir os dados GrupoProdutos.', duration: 3000, position: 'botton' }).present();
            });
        }
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllGrupoProdutos() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from grupoProdutos', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let grupoProdutos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var grupoProduto = data.rows.item(i);
              grupoProdutos.push(grupoProduto);
            }
            return grupoProdutos;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/SubGrupoProdutos\/----------------------------------------------
  public insertSubGrupoProdutos(listaSubGrupoProdutos){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE subGrupoProdutos';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS subGrupoProdutos (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, idSubGrupoProdutos TEXT, grupo TEXT, label1 TEXT, status TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaSubGrupoProdutos.length; i++) {
          let sql = 'insert into subGrupoProdutos (nome, idSubGrupoProdutos, grupo, label1, status) values (?, ?, ?, ?, ?)';
          let data = [listaSubGrupoProdutos[i].nome, listaSubGrupoProdutos[i].id, listaSubGrupoProdutos[i].grupo, 
          listaSubGrupoProdutos[i].label1, listaSubGrupoProdutos[i].status];
      
          db.executeSql(sql, data)
            .catch(() => {
              // this.toast.create({ message: 'Erro ao incluir os dados SubGrupoProdutos.', duration: 3000, position: 'botton' }).present();
            });
        }
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllSubGrupoProdutos() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from subGrupoProdutos', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let subGrupoProdutos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var subGrupoProduto = data.rows.item(i);
              subGrupoProdutos.push(subGrupoProduto);
            }
            return subGrupoProdutos;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/Produtos\/----------------------------------------------
  public insertProdutos(listaProdutos){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE produtos';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS produtos (id TEXT, nome TEXT, grupo TEXT, subgrupo TEXT, unidade TEXT, url TEXT, status TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaProdutos.length; i++) {
          let sql = 'insert into produtos (id, nome, grupo, subgrupo, unidade, url, status) values (?, ?, ?, ?, ?, ?, ?)';
          let data = [listaProdutos[i].id, listaProdutos[i].nome, listaProdutos[i].grupo, listaProdutos[i].subgrupo, 
          listaProdutos[i].unidade, listaProdutos[i].url, listaProdutos[i].status];
      
          db.executeSql(sql, data)
            .catch(() => {
              // this.toast.create({ message: 'Erro ao incluir os dados Produtos.', duration: 3000, position: 'botton' }).present();
            });
        }
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllProdutos() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from produtos', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let produtos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var produto = data.rows.item(i);
              produtos.push(produto);
            }
            return produtos;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/Agenda\/----------------------------------------------
  public insertAgendas(listaAgendas){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE agendas';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS agendas (datainicio TEXT, observacao TEXT, nomecliente TEXT, descricaopiscina TEXT, idpiscina TEXT, idpiscineiro TEXT, idcliente TEXT, nomepiscineiro TEXT, avaliada TEXT, formatopiscina TEXT, datatermino TEXT, id TEXT, dataagendamento TEXT, status TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaAgendas.length; i++) {
          let sql = 'insert into agendas (datainicio, observacao, nomecliente, descricaopiscina, idpiscina, idpiscineiro, idcliente, nomepiscineiro, avaliada, formatopiscina, datatermino, id, dataagendamento, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
          let data = [listaAgendas[i].datainicio, listaAgendas[i].observacao, listaAgendas[i].nomecliente, 
          listaAgendas[i].descricaopiscina, listaAgendas[i].idpiscina, listaAgendas[i].idpiscineiro, 
          listaAgendas[i].idcliente, listaAgendas[i].nomepiscineiro, listaAgendas[i].avaliada, listaAgendas[i].formatopiscina, 
          listaAgendas[i].datatermino, listaAgendas[i].id, listaAgendas[i].dataagendamento, listaAgendas[i].status];
      
          db.executeSql(sql, data)
            .catch(() => {
              // this.toast.create({ message: 'Erro ao incluir os dados Agenda.', duration: 3000, position: 'botton' }).present();
            });
        }
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllAgendas() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from agendas', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let agendas: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var agenda = data.rows.item(i);
              agendas.push(agenda);
            }
            return agendas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/Piscinas\/----------------------------------------------
  public insertPiscinas(listaPiscinas){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE piscinas';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS piscinas (id TEXT, descricao TEXT, formato TEXT, logradouro TEXT, numero TEXT, bairro TEXT, cep TEXT, cidade TEXT, siglaunidadefederativa TEXT, pais TEXT, tipoendereco TEXT, clienteId TEXT, volumeagua INTEGER, status TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaPiscinas.length; i++) {
          let sql = 'insert into piscinas (id, descricao, formato, logradouro, numero, bairro, cep, cidade, siglaunidadefederativa, pais, tipoendereco, clienteId, volumeagua, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
          let data = [listaPiscinas[i].id, listaPiscinas[i].descricao, listaPiscinas[i].formato, 
          listaPiscinas[i].endereco.logradouro, listaPiscinas[i].endereco.numero, listaPiscinas[i].endereco.bairro, 
          listaPiscinas[i].endereco.cep, listaPiscinas[i].endereco.cidade.nome, listaPiscinas[i].endereco.cidade.unidadeFederativa.sigla, 
          listaPiscinas[i].endereco.cidade.unidadeFederativa.pais.nome, listaPiscinas[i].endereco.tipoendereco, listaPiscinas[i].clienteId, listaPiscinas[i].volumeagua, listaPiscinas[i].status];
      
          db.executeSql(sql, data)
            .catch(() => {
              // this.toast.create({ message: 'Erro ao incluir os dados Piscinas.', duration: 3000, position: 'botton' }).present();
            });
        }
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllPiscinas() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from piscinas', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let piscinas: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var piscina = data.rows.item(i);
              piscinas.push(piscina);
            }
            return piscinas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/ServicosAgendas\/----------------------------------------------
  public insertServicosAgendas(listaServicosAgenda){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE servicosAgendas';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS servicosAgendas (idAgenda TEXT, id TEXT, nome TEXT, subgrupo TEXT, statusagendaservico TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaServicosAgenda.length; i++) {
          for (let j = 0; j < listaServicosAgenda[i].data.length; j++) {
            if (listaServicosAgenda[i].data[j].statusagendaservico == "NAOATENDIDO") {
              let sql = 'insert into servicosAgendas (idAgenda, id, nome, subgrupo, statusagendaservico) values (?, ?, ?, ?, ?)';
              let data = [listaServicosAgenda[i].data[j].agendaid, listaServicosAgenda[i].data[j].id, listaServicosAgenda[i].data[j].nome, 
              listaServicosAgenda[i].data[j].subgrupo, listaServicosAgenda[i].data[j].statusagendaservico];
          
              db.executeSql(sql, data)
                .catch(() => {
                  // this.toast.create({ message: 'Erro ao incluir os dados ServicosAgenda.', duration: 3000, position: 'botton' }).present();
                });
            } 
          }
        }
      })
      .catch(()=>{
        // this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllServicosAgendas() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from servicosAgendas', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let piscinas: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var piscina = data.rows.item(i);
              piscinas.push(piscina);
            }
            return piscinas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/Fotos\/----------------------------------------------
  public insertFotos(listaFotos){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE fotos';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS fotos (id integer primary key AUTOINCREMENT NOT NULL, base64 TEXT, idAgenda TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

        for (let i = 0; i < listaFotos.imagens.length; i++) {
          let sql = 'insert into fotos (base64, idAgenda) values (?, ?)';
          let data = [listaFotos.imagens[i].url, listaFotos.id];

          db.executeSql(sql, data)
            .then(()=>{
              this.toast.create({ message: listaFotos.id, duration: 3000, position: 'botton' }).present();
            })
            .catch(() => {
              this.toast.create({ message: 'Erro ao incluir os dados Fotos.', duration: 3000, position: 'botton' }).present();
            });
        }
      })
      .catch(()=>{
        this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllFotos() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from fotos', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let fotos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var foto = data.rows.item(i);
              fotos.push(foto);
            }
            return fotos;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }  

  // -----------------------------\/Finalizar Agenda\/----------------------------------------------
  public insertFinalizarAgenda(finalizarAgenda){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        
        let sql = 'insert into finalizarAgenda (id, observacao, datainicio, datatermino, status) values (?, ?, ?, ?, ?)';
        let data = [finalizarAgenda.id, finalizarAgenda.observacao, finalizarAgenda.datainicio, 
        finalizarAgenda.datatermino, finalizarAgenda.status];
    
        db.executeSql(sql, data)
          .catch(() => {
            this.toast.create({ message: 'Erro ao incluir os dados Finalizar Agenda.', duration: 3000, position: 'botton' }).present();
          });
      })
      .catch(()=>{
        this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getAllFinalizarAgenda() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS finalizarAgenda (id TEXT, observacao TEXT, datainicio TEXT, datatermino TEXT, status TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

      return db.executeSql('select * from finalizarAgenda', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let finalizarAgendas: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var finalizarAgenda = data.rows.item(i);
              finalizarAgendas.push(finalizarAgenda);
            }
            return finalizarAgendas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/Produtos Sugeridos\/----------------------------------------------
  public insertProdutosSugeridos(produtosSugeridos){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {

        for (let i = 0; i < produtosSugeridos.items.length; i++) {
          let sql = 'insert into produtosSugeridos (id, idAgenda, quantidade) values (?, ?, ?)';
          let data = [produtosSugeridos.items[i].produto.id, produtosSugeridos.idAgenda, produtosSugeridos.items[i].quantidade];
      
          db.executeSql(sql, data)
            .catch(() => {
              this.toast.create({ message: 'Erro ao incluir os dados Produtos Sugeridos.', duration: 3000, position: 'botton' }).present();
            });
        }
        
      })
      .catch(()=>{
        this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getProdutosSugeridos(idAgenda) {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
      
      let sql = 'DELETE TABLE pessoas';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS produtosSugeridos (id TEXT, idAgenda TEXT, quantidade TEXT)'],
          ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  

      return db.executeSql('select * from produtosSugeridos', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let produtosSugeridos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var produtosSugerido = data.rows.item(i);
              produtosSugeridos.push(produtosSugerido);
            }
            return produtosSugeridos;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/Servicos Atendidos\/----------------------------------------------
  public insertServicosAtendidos(servicosAtendidos){

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE TABLE pessoas';
        db.executeSql(sql)

        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS servicosAtendidos (id TEXT, idAgenda TEXT, status TEXT)']
        ])
          .then(() => {
            // this.toast.create({ message: 'Tabelas Criadas.', duration: 3000, position: 'botton' }).present();
          })
          .catch(()=>{
            // this.toast.create({ message: 'Erro ao criar as Tabelas.', duration: 3000, position: 'botton' }).present();
          });  
          
        for (let i = 0; i < servicosAtendidos.servicos.length; i++) {
          let sql = 'insert into servicosAtendidos (id, idAgenda, status) values (?, ?, ?)';
          let data = [servicosAtendidos.servicos[i].servico.id, servicosAtendidos.idAgenda, servicosAtendidos.servicos[i].status];
      
          db.executeSql(sql, data)
            .catch(() => {
              // this.toast.create({ message: 'Erro ao incluir os dados Servicos Atendidos.', duration: 3000, position: 'botton' }).present();
            });
        }
        
      })
      .catch(()=>{
        this.toast.create({ message: 'Erro ao acessar o Banco.', duration: 3000, position: 'botton' }).present();
      });
  }

  public getServicosAtendidos(idAgenda) {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from servicosAtendidos', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let servicosAtendidos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var servicosAtendido = data.rows.item(i);
              servicosAtendidos.push(servicosAtendido);
            }
            return servicosAtendidos;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

  // -----------------------------\/Delete dados Agendas Atendidas\/----------------------------------------------
  public deleteAgenda(agenda){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        db.executeSql('DELETE FROM finalizarAgenda WHERE id = ?', [agenda.id])
        .then(() => {
          // this.toast.create({ message: 'Deletado com Suscesso', duration: 3000, position: 'botton' }).present();
        })
        .catch((e) => {
          let erro = e.message
          // this.toast.create({ message: erro, duration: 3000, position: 'botton' }).present();
        });

        db.executeSql('DELETE FROM produtosSugeridos WHERE idAgenda = ?', [agenda.id])
        .then(() => {
          // this.toast.create({ message: 'Deletado com Suscesso', duration: 6000, position: 'botton' }).present();
        })
        .catch((e) => {
          let erro = e.message
          // this.toast.create({ message: erro, duration: 6000, position: 'botton' }).present();
        });

        db.executeSql('DELETE FROM servicosAtendidos WHERE idAgenda = ?', [agenda.id])
        .then(() => {
          // this.toast.create({ message: 'Deletado com Suscesso', duration: 9000, position: 'botton' }).present();
        })
        .catch((e) => {
          let erro = e.message
          // this.toast.create({ message: erro, duration: 9000, position: 'botton' }).present();
        });
      })
      .catch((e) => console.error(e));
  }

  // -----------------------------\/Update dados Agendas Atendidas\/----------------------------------------------
  public updateAgenda(agenda){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        
        db.executeSql('UPDATE agendas SET datainicio=?, observacao=?, datatermino=?, status=? WHERE id=?', [agenda.datainicio, agenda.observacao, agenda.datatermino, agenda.status, agenda.id])
        .then(() => {
          // this.toast.create({ message: 'Update com Suscesso', duration: 3000, position: 'botton' }).present();
        })
        .catch((e) => {
          let erro = e.message
          // this.toast.create({ message: erro, duration: 20000, position: 'botton' }).present();
        });
      })
      .catch((e) => console.error(e));
  }
}


import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class LoginProvider {

  constructor(private dbProvider: DatabaseProvider) {
    
  }

  public getAll() {
    return this.dbProvider.getDB()
    .then((db: SQLiteObject) => {
 
      return db.executeSql('select * from login', [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let logins: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var login = data.rows.item(i);
              logins.push(login);
            }
            return logins;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
  }

}

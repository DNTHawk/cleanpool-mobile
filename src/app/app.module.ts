import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { CleanPool } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera'
import { BrMaskerModule } from 'brmasker-ionic-3';
import { Network } from '@ionic-native/network';
import { SQLite } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../providers/database/database';
import { LoginProvider } from '../providers/login/login';
import { BaseDadosOffProvider } from '../providers/base-dados-off/base-dados-off';

@NgModule({
  declarations: [
    CleanPool,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(CleanPool),
    HttpModule,
    BrMaskerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    CleanPool
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Geolocation,
    Camera,
    Network,
    SQLite,
    DatabaseProvider,
    LoginProvider,
    BaseDadosOffProvider
  ]
})
export class AppModule { }

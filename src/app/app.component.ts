import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen'
import { HomeClientePage } from '../pages/home-cliente/home-cliente';

@Component({
  templateUrl: 'app.html'
})
export class CleanPool {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'TelaLoginPage';

  pages: Array<{ title: string, component: any }>;
  pagesCliente: Array<{ title: string, component: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    this.pages = [
    ];

    this.pagesCliente = [
      { title: 'Produtos', component: 'SelecionarPiscinaPage' },
      { title: 'Agenda de Visitas', component: 'AgendaClientePage' },
      { title: 'Conteúdos', component: 'ConteudoPage' }
    ];

    platform.ready().then(() => {
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#488aff');

      splashScreen.hide();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.push(page.component);
  }
  openPagePerfil() {
    this.nav.setRoot('TelaPerfilPage');
  }
  openPageHomeCliente() {
    this.nav.setRoot('HomeClientePage');
  }

  sair() {
    localStorage.clear()   

    this.nav.setRoot('TelaLoginPage');
  }
}
